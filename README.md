[![Build Status](https://drone.io/bitbucket.org/jpathy/raft/status.png)](https://drone.io/bitbucket.org/jpathy/raft/latest)

# About #

This is Yet Another Raft Implementation in Go.

## Features ##

* Pipelining (untested)
* Two-phase Membership changes (tested)
* Log compaction (untested)

## TODO ##

* Testing

## Usage ##

There is an example in testing.

## Docs ##

[![GoDoc](https://godoc.org/bitbucket.org/jpathy/raft?status.svg)](https://godoc.org/bitbucket.org/jpathy/raft)

### LICENSE ###

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)
