package raft

const (
	// AppendReqRPC is Kind() of AppendEntriesRequest.
	AppendReqRPC = iota
	// AppendRespRPC is Kind() of AppendEntriesResponse.
	AppendRespRPC

	// VoteReqRPC is Kind() of RequestVoteRequest.
	VoteReqRPC
	// VoteRespRPC is Kind() of RequestVoteResponse.
	VoteRespRPC

	// SnapReqRPC is Kind() of SnapshotRequest.
	SnapReqRPC
	// SnapRespRPC is Kind() of SnapshotResponse.
	SnapRespRPC

	nRPCs
)

// RPC is the interface of all raft messages.
type RPC interface {
	Kind() uint8
}

// AppendEntriesRequest represents an append request.
type AppendEntriesRequest struct {
	Term         uint64
	PrevTerm     uint64
	PrevIdx      uint64
	LeaderCommit uint64
	Entries      []*Entry
}

// Kind implements RPC.
func (m *AppendEntriesRequest) Kind() uint8 {
	return AppendReqRPC
}

// AppendEntriesResponse represents the response to an append request.
// Index = AppendEntriesRequest.PrevIdx+len(Entries) i.e. the index of last entry in the Request.(Useful for pipelining) for Success = true.
type AppendEntriesResponse struct {
	Term    uint64
	Index   uint64
	Success bool
}

// Kind implements RPC.
func (m *AppendEntriesResponse) Kind() uint8 {
	return AppendRespRPC
}

// RequestVoteRequest represents a vote request.
type RequestVoteRequest struct {
	Pre         bool
	Term        uint64
	LastLogIdx  uint64
	LastLogTerm uint64
}

// Kind implements RPC.
func (m *RequestVoteRequest) Kind() uint8 {
	return VoteReqRPC
}

// RequestVoteResponse represents response to vote request.
type RequestVoteResponse struct {
	Pre         bool
	Term        uint64
	VoteGranted bool
}

// Kind implements RPC.
func (m *RequestVoteResponse) Kind() uint8 {
	return VoteRespRPC
}

// SnapshotRequest represents a snapshot request.
type SnapshotRequest struct {
	Term     uint64
	SnapPos  uint64
	SnapData []byte
	Done     bool
}

// Kind implements RPC.
func (m *SnapshotRequest) Kind() uint8 {
	return SnapReqRPC
}

// SnapshotResponse represents response to snapshot request.
// SnapPos = req.SnapPos + len(SnapData). (useful for pipelining).
type SnapshotResponse struct {
	Term    uint64
	SnapPos uint64
}

// Kind implements RPC.
func (m *SnapshotResponse) Kind() uint8 {
	return SnapRespRPC
}

func mkAppendEntriesRequest(term, prevTerm, prevIdx, commitIdx uint64, e []*Entry) (m RPC) {
	m = &AppendEntriesRequest{
		Term:         term,
		PrevTerm:     prevTerm,
		PrevIdx:      prevIdx,
		LeaderCommit: commitIdx,
		Entries:      e,
	}
	return
}

func mkAppendEntriesResponse(term, index uint64, ok bool) (m RPC) {
	m = &AppendEntriesResponse{
		Term:    term,
		Index:   index,
		Success: ok,
	}
	return
}

func mkRequestVoteRequest(term, lastIdx, lastTerm uint64, pre bool) (m RPC) {
	m = &RequestVoteRequest{
		Pre:         pre,
		Term:        term,
		LastLogIdx:  lastIdx,
		LastLogTerm: lastTerm,
	}
	return
}

func mkRequestVoteResponse(term uint64, ok, pre bool) (m RPC) {
	m = &RequestVoteResponse{
		Pre:         pre,
		Term:        term,
		VoteGranted: ok,
	}
	return
}

func mkSnapshotRequest(term uint64, b []byte, pos uint64, done bool) (m RPC) {
	m = &SnapshotRequest{
		Term:     term,
		SnapData: b,
		SnapPos:  pos,
		Done:     done,
	}
	return
}

func mkSnapshotResponse(term uint64, pos uint64) (m RPC) {
	m = &SnapshotRequest{
		Term:    term,
		SnapPos: pos,
	}
	return
}
