package raft

import (
	"fmt"
	"testing"
)

const s1 = "Hello "
const s2 = "World"

type TestCommand1 string
type TestCommand2 string

func (t *TestCommand1) ID() uint8 {
	return 5
}

func (t *TestCommand1) Size() int {
	return len(s1)
}

func (t *TestCommand1) MarshalTo(b []byte) (n int, err error) {
	n = copy(b, []byte(s1))
	return
}

func (t *TestCommand1) Unmarshal(b []byte) (err error) {
	if len(b) < len(s1) {
		err = fmt.Errorf("bytes slice too short")
		return
	}

	s := string(b[:len(s1)])
	if s != s1 {
		return fmt.Errorf("read command is not %s", s1)
	}
	*t = TestCommand1(s)
	return
}

func (t *TestCommand2) ID() uint8 {
	return 8
}

func (t *TestCommand2) Size() int {
	return len(s2)
}

func (t *TestCommand2) MarshalTo(b []byte) (n int, err error) {
	n = copy(b, []byte(s2))
	return
}

func (t *TestCommand2) Unmarshal(b []byte) (err error) {
	if len(b) < len(s2) {
		err = fmt.Errorf("byte slice too short")
		return
	}

	s := string(b[:len(s2)])
	if s != s2 {
		return fmt.Errorf("read command is not %s", s2)
	}
	*t = TestCommand2(s)
	return
}

func TestCommands(t *testing.T) {
	var t1 TestCommand1
	var t2 TestCommand2
	if err := RegisterCommands(&t1, &t2); err != nil {
		t.Error(err)
	}
	c := &ClientCommand{900, "Client1", &t1}
	b, err := c.marshal()
	if err != nil {
		t.Error(err)
	}
	if err = c.unmarshal(b); err != nil {
		t.Error(err)
	}
	if c.seq != 900 {
		t.Errorf("Sequence doesn't match %d", c.seq)
	}
	if c.clientID != "Client1" {
		t.Errorf("ClientID doesn't match")
	}
	if nt1, ok := c.cmd.(*TestCommand1); !ok {
		t.Errorf("Command → Read %d; Should be %d", c.cmd.ID(), t1.ID())
	} else if *nt1 != TestCommand1(s1) {
		t.Errorf("Doesn't match Marshalled Command")
	}

	e, err := mkEntry(1, c)
	if err != nil {
		t.Error("Failed to create Entry")
	}
	if e.Term() != 1 {
		t.Error("Entry's Terms don't match")
	}

	if clcmd, err := e.clientCommand(); err != nil {
		t.Error(err)
	} else if clcmd.seq != 900 || clcmd.clientID != "Client1" {
		t.Error("Unmarshalled Entry's ClientCommand doesn't match")
	}
}
