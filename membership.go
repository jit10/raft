package raft

// Zero, Old & New Configuration.
const zC = iota

const (
	oC = 1 << iota
	nC
)

// Element of.
type ɛ uint32

func (m *ɛ) ɛCnil() bool {
	return *m == zC
}

func (m *ɛ) ɛCold() bool {
	return *m&oC == oC
}

func (m *ɛ) ɛCnew() bool {
	return *m&nC == nC
}

func (m *ɛ) SetɛCold() {
	*m |= oC
}

func (m *ɛ) SetɛCnew() {
	*m |= nC
}

func (m *ɛ) SetɛCnil() {
	*m = zC
}
