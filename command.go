package raft

import (
	"encoding/binary"
	"fmt"
	"reflect"
)

const (
	commandIDBytes  = 1
	maxCommandTypes = commandIDBytes*(1<<8) - 1
)

var commandTypes [maxCommandTypes]reflect.Type

// Command is the interface to be implemented by statemachine commands which are replicated under raft.
// Implementation of Command MUST be Pure (version it, if encoding may change in future)
type Command interface {
	ID() uint8
	Size() int
	MarshalTo(b []byte) (n int, err error)
	Unmarshal(b []byte) error
}

func isRaftCmd(id uint8) bool {
	return id < ReservedIDs
}

// ClientCommand represents the Command issued by a client.
type ClientCommand struct {
	seq      uint64
	clientID string
	cmd      Command
}

// Sequence returns the sequence no. of this ClientCommand.
func (c *ClientCommand) Sequence() uint64 {
	return c.seq
}

// ClientID returns the ID of the client who issued this ClientCommand.
func (c *ClientCommand) ClientID() string {
	return c.clientID
}

// Command returns the Command contained in this ClientCommand.
func (c *ClientCommand) Command() Command {
	return c.cmd
}

// MkClientCommand returns a new ClientCommand. seq and clientID enables the StateMachine to deal with duplicate requests when Commands are not idempotent.
func MkClientCommand(seq uint64, clientID string, cmd Command) *ClientCommand {
	clcmd := &ClientCommand{
		seq:      seq,
		clientID: clientID,
		cmd:      cmd,
	}

	return clcmd
}

// isRaftCommand tests for command to be a raft command type used internally.
func (c *ClientCommand) isRaftCommand() bool {
	return isRaftCmd(c.cmd.ID())
}

// Marshal to be written to log. This is Pure.
func (c *ClientCommand) marshal() (b []byte, err error) {
	l := len(c.clientID)
	var ba [2 * binary.MaxVarintLen64]byte
	bs := ba[:]
	k := binary.PutUvarint(bs, c.seq)
	k += binary.PutUvarint(bs[k:], uint64(l))

	size := c.cmd.Size() + commandIDBytes + k + l
	b = make([]byte, size)

	// First byte(s) is/are command type.
	// Change the following line if commandIDBytes > 1
	b[0] = c.cmd.ID()
	pos := commandIDBytes

	// Now the seq and clientID.
	copy(b[pos:], bs[:k])
	pos += k
	copy(b[pos:], []byte(c.clientID))
	pos += l

	// Now the Command itself.
	n, err := c.cmd.MarshalTo(b[pos:])
	if err != nil {
		return
	}
	pos += n
	b = b[:pos]

	return
}

// Unmarshal unmarshals a ClientCommand.
func (c *ClientCommand) unmarshal(b []byte) error {
	if len(b) == 0 {
		return fmt.Errorf("failed to Unmarshal from Empty buffer")
	}

	// Change the following line if commandIDBytes > 1
	id := uint8(b[0])
	pos := commandIDBytes

	// Read the seq.
	seq, k := binary.Uvarint(b[pos:])
	if k <= 0 {
		return fmt.Errorf("failed to read the seq No")
	}
	c.seq = seq
	pos += k

	// Read the len(c.clientID).
	l, k := binary.Uvarint(b[pos:])
	if k <= 0 {
		return fmt.Errorf("failed to read the length of clientID")
	}
	pos += k

	// Read the c.clientID.
	if len(b[pos:]) < int(l) {
		return fmt.Errorf("failed to read the clientID")
	}
	c.clientID = string(b[pos : pos+int(l)])
	pos += int(l)

	ctype := commandTypes[id]
	if ctype == nil {
		return fmt.Errorf("read Command ID : %d is not registered", id)
	}

	var v reflect.Value
	if ctype.Kind() != reflect.Ptr {
		v = reflect.Indirect(reflect.New(ctype))
	} else {
		v = reflect.New(ctype.Elem())
	}
	cmd, ok := v.Interface().(Command)
	if !ok {
		return fmt.Errorf("command ID: %d doesn't implement Unmarshal", id)
	}

	if err := cmd.Unmarshal(b[pos:]); err != nil {
		return err
	}

	c.cmd = cmd
	return nil
}

// Entry represents a log Entry.
type Entry struct {
	term uint64
	buf  []byte
}

// Term returns contained term of the Entry.
func (e *Entry) Term() uint64 {
	return e.term
}

// Marshal returns marshalled Entry.
func (e *Entry) Marshal() (b []byte, err error) {
	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]

	k := binary.PutUvarint(bs, e.term)
	b = append(bs[:k], e.buf...)

	return
}

// Unmarshal unmarshals the buffer into log Entry.
func (e *Entry) Unmarshal(b []byte) error {
	if e == nil {
		return fmt.Errorf("nil Entry")
	}

	// Read the Term.
	term, k := binary.Uvarint(b)
	if k <= 0 {
		return fmt.Errorf("failed to read the Term")
	}

	e.term = term
	e.buf = append(e.buf, b[k:]...)
	return nil
}

// GobEncode implements gob.GobEncoder.
func (e *Entry) GobEncode() ([]byte, error) {
	return e.Marshal()
}

// GobDecode implements gob.GobDecoder.
func (e *Entry) GobDecode(b []byte) error {
	return e.Unmarshal(b)
}

// mkEntry creates a log Entry from the term and ClientCommand.
func mkEntry(term uint64, clcmd *ClientCommand) (e *Entry, err error) {
	b, err := clcmd.marshal()
	if err != nil {
		return
	}

	e = &Entry{
		term: term,
		buf:  b,
	}
	return
}

// tests if log Entry is Raft Command without actually unmarshalling it.
func (e *Entry) isRaftCommand() bool {
	if len(e.buf) == 0 {
		debug.Println("Length of Entry shouldn't be zero")
		return false
	}

	// Change the following if commandIDBytes > 1
	id := e.buf[0]

	return isRaftCmd(id)
}

// return the ClientCommand associated.
func (e *Entry) clientCommand() (clcmd *ClientCommand, err error) {
	var c ClientCommand
	if err = c.unmarshal(e.buf); err == nil {
		clcmd = &c
	}

	return
}

// returns the command associated.
func (e *Entry) command() (cmd Command, err error) {
	var c ClientCommand
	if err = c.unmarshal(e.buf); err == nil {
		cmd = c.cmd
	}

	return
}

// RegisterCommands records the type of command to be used. Must be done apriori for state machine commands.
// commands with ID() < raftproto.ReservedIDs returns error.
func RegisterCommands(cmds ...Command) error {
	for _, cmd := range cmds {
		id := cmd.ID()
		if id >= maxCommandTypes {
			return fmt.Errorf("command ID %d can't be greater than %d", id, maxCommandTypes)
		}

		ctype := reflect.TypeOf(cmd)
		if commandTypes[id] != nil {
			return fmt.Errorf("%v conflicting ID: %d with already present %v", ctype, id, commandTypes[id])
		}
		commandTypes[id] = ctype
	}
	return nil
}
