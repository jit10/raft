package raft

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

const (
	dialTimeOut = 20 * time.Millisecond
)

type tcpComm struct {
	laddr string
	net.Listener
	timeout time.Duration
}

func (comm *tcpComm) String() string {
	return "tcp"
}

func (comm *tcpComm) Dial(raddr string) (t Transporter, err error) {
	c, err := net.DialTimeout("tcp", raddr, comm.timeout)

	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]
	sl := binary.PutUvarint(bs, uint64(len(comm.laddr)))
	bs = append(bs[:sl], []byte(comm.laddr)...)
	if _, err = c.Write(bs); err != nil {
		return
	}

	if err == nil {
		t = NewNetTransporter(raddr, c)
	}

	return
}

func (comm *tcpComm) Accept() (t Transporter, err error) {
	c, err := comm.Listener.Accept()
	if err != nil {
		return
	}

	var ba [binary.MaxVarintLen64]byte
	var slen uint64
	bs := ba[:]
	k, _ := c.Read(bs)
	if k > 0 {
		slen, k = binary.Uvarint(bs[:k])
	}
	if k <= 0 {
		err = fmt.Errorf("failed to read length of uid")
		return
	}
	bs = append(bs, make([]byte, int(slen)-len(ba)+k)...)
	if _, err = c.Read(bs[len(ba):]); err != nil {
		return
	}

	uid := string(bs[k:])
	if err == nil {
		t = NewNetTransporter(uid, c)
	}
	return
}

func (comm *tcpComm) Close() error {
	return comm.Listener.Close()
}

func newTcpComm(uid string) (Communication, error) {
	conlr, err := net.Listen("tcp", uid)
	if err != nil {
		return nil, err
	}

	comm := &tcpComm{
		laddr:    uid,
		Listener: conlr,
	}

	return comm, nil
}
