package raft

import (
	"bytes"
	"os"
	"testing"
)

const raftPath = "/tmp/_raftTest"

func TestAppendEntries(t *testing.T) {
	l, err := openDefaultLog(raftPath, 100)
	if err != nil {
		t.Error(err)
	}
	entries := [][]byte{[]byte{1, 2, 3}, []byte{3, 4, 8, 12}, []byte{9, 5, 15}}

	term := uint64(999)
	for i := range entries {
		_, err = l.WriteEntry(&Entry{term, entries[i]})
		if err != nil {
			t.Error(err)
		}
	}
	if err = l.FlushEntries(); err != nil {
		t.Error(err)
	}
	if err = l.Close(); err != nil {
		t.Error(err)
	}

	/* Open again */
	l, err = openDefaultLog(raftPath, 100)
	if err != nil {
		t.Error(err)
	}

	if uint64(len(entries)) != l.LastIndex() {
		t.Error("Entries count doesn't match")
	}

	for i := range entries {
		e, err := l.EntryAt(uint64(i) + l.FirstIndex())
		if err != nil {
			t.Error(err)
			break
		}
		if e.term != term || bytes.Compare(e.buf, entries[i+int(l.FirstIndex())-1]) != 0 {
			t.Errorf("Entry %d doesn't match", i+int(l.FirstIndex()))
			break
		}
	}

	if es, err := l.Entries(1, 4, 0); err != nil {
		t.Error(err)
	} else {
		for i := range es {
			if es[i].term != term || bytes.Compare(es[i].buf, entries[i+int(l.FirstIndex())-1]) != 0 {
				t.Errorf("Entry %d doesn't match", i+int(l.FirstIndex()))
			}
		}
	}

	if err := l.TruncateFrom(3); err != nil {
		t.Error(err)
	}

	if err := l.TruncateUpto(2); err != nil {
		t.Error(err)
	}

	if _, err := l.Entries(1, 4, 0); err != ErrSnapRd {
		t.Error(err)
	}

	// After truncate
	if bs, err := l.Entries(2, 4, 0); err != nil {
		t.Error(err)
	} else {
		if len(bs) != 1 {
			t.Errorf("Truncation failure")
		}
		for i := range bs {
			if bs[i].term != term || bytes.Compare(bs[i].buf, entries[i+int(l.FirstIndex())-1]) != 0 {
				t.Errorf("Entry %d doesn't match", i+int(l.FirstIndex()))
			}
		}
	}

	if err = l.Close(); err != nil {
		t.Error(err)
	}

	os.RemoveAll(raftPath)
}
