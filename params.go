package raft

import (
	"encoding/json"
	"fmt"
	"io"
)

// ConfigParams represensts set of initial server parameters.
type ConfigParams struct {
	ServerID     string `json:"ServerID"`
	BatchLimit   int    `json:"BatchLimitPerRequest"`
	MaxReqSize   int    `json:"SoftMessageSizeLimit"`
	MaxEntries   uint32 `json:"MaxEntriesToSnapshot"`
	SnapThresold uint32 `json:"EntriesToKeepAfterSnapshot"`
	BaseTimeUnit uint32 `json:"UnitTimeInμs"`
	ElectTmUnits uint32 `json:"ElectionTimeUnits"`
	LogPath      string `json:"DefaultLogPath"`
}

// Marshal encodes ConfigParams as json object into io.Writer.
func (params *ConfigParams) Marshal(w io.Writer) error {
	return json.NewEncoder(w).Encode(params)
}

// Unmarshal decodes ConfigParams from Reader.
func (params *ConfigParams) Unmarshal(r io.Reader) error {
	if params == nil {
		return fmt.Errorf("Nill parameter")
	}

	return json.NewDecoder(r).Decode(params)
}
