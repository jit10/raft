package raft

import (
	"fmt"
	"testing"
)

type member struct {
	ɛ
}

func TestMembership(t *testing.T) {
	var m member

	m.SetɛCnil()
	if !m.ɛCnil() || m.ɛCold() || m.ɛCnew() {
		t.Error(fmt.Errorf("must belong to Cnil only"))
	}

	m.SetɛCold()
	if !m.ɛCold() {
		t.Error(fmt.Errorf("must belong to Cold"))
	}

	m.SetɛCnew()
	if !m.ɛCnew() {
		t.Error(fmt.Errorf("must belong to Cnew"))
	}

	if !m.ɛCnew() || !m.ɛCold() || m.ɛCnil() {
		t.Error(fmt.Errorf("must belong to both Cnew Cold but not Cnil"))
	}

	m.SetɛCnil()
	if !m.ɛCnil() {
		t.Error(fmt.Errorf("must belong to Cnil"))
	}
}
