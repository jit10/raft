package raft

import (
	"fmt"
	"sort"
	"sync"
	"sync/atomic"
)

type comparable interface {
	Less(interface{}) bool
}

// Satisfy sort.Interface
type comparableSlice []comparable

func (s comparableSlice) Len() int {
	return len(s)
}

func (s comparableSlice) Less(i, j int) bool {
	return s[i].Less(s[j])
}

func (s comparableSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s comparableSlice) Search(x comparable) (n int) {
	return sort.Search(len(s), func(i int) bool { return !s[i].Less(x) })
}

type server interface {
	RequestVote(actual bool) bool
	AppendEntries(force bool) bool

	UID() string

	VoteGranted() bool

	MatchIndex() uint64
	ResetState()

	Reset(Transporter) error
	Close() error

	ɛCold() bool
	ɛCnew() bool
	SetɛCnil()
	SetɛCold()
	SetɛCnew()
}

// Raft Configuration States.
const (
	NILL = iota
	JOINT
	STABLE

	nConfigStates
)

var cstateString = [...]string{"Nill", "Joint Consensus", "Stable"}

// ConfigState type represents raft configuration states.
type ConfigState uint32

func (s ConfigState) String() string {
	if s < nConfigStates {
		return cstateString[s]
	}
	return fmt.Sprintf("Invalid")
}

type configurator interface {
	GetState() ConfigState

	SetSnapshot(index uint64, conf *Configuration)
	GetCurrentConfig() *Configuration

	Set(index uint64, conf *Configuration) bool
	GetIndex(index uint64) uint64
	Get(index uint64) *Configuration

	// TruncateFrom confIndex(including), where confIdx >= index.
	TruncateFrom(index uint64) error
	// TruncateUpto confIndex(excluding), where confIdx <= index.
	TruncateUpto(index uint64) error

	// Adds a server with nil membership.
	AddServer(uid string, t Transporter) server
	// Returns true if s belongs to either Cold or Cnew.
	Contains(s server) bool
	// Removes servers with nil membership.
	GCServers()

	HasMajority(f func(s server) bool) bool
	GetMajority(f func(s server) comparable) comparable

	// Invokes f on s corresponding to uid.
	Invoke(uid string, f func(s server))
	// Invokes f on s. In parallel if par == true.
	InvokeAll(par bool, f func(s server))
}

// Satisifies configurator interface.
type configMgr struct {
	sync.Mutex
	state ConfigState

	servers map[string]server

	// We can use a simple slice, since there shouldn't be crazy no. of reconfiguration between snapshots.
	log []uint64

	oConf []string
	nConf []string

	// Snapshot configuration.
	snapIdx    uint64
	snapConfig *Configuration

	// the host of this Configuration.
	host *replica

	// to avoid allocations
	ovals []comparable
	nvals []comparable
}

func (cfg *configMgr) addServer(uid string, t Transporter) (s server) {
	s, ok := cfg.servers[uid]
	if !ok {
		// Create the new peer on the replica.
		s = cfg.host.addPeer(uid, t)
		cfg.servers[uid] = s
	} else if ok && t != nil {
		// Reset peer with this new Connection.
		if err := s.Reset(t); err != nil {
			debug.Println(err)
		}
	}

	return s
}

func (cfg *configMgr) contains(s server) bool {
	switch cfg.GetState() {
	case STABLE:
		return s.ɛCold()
	case JOINT:
		return s.ɛCold() || s.ɛCnew()
	}

	return false
}

func (cfg *configMgr) cleanUp() {
	if !cfg.contains(cfg.host) {
		for id, s := range cfg.servers {
			if err := s.Close(); err != nil {
				debug.Println(err)
			}
			delete(cfg.servers, id)
		}
	} else {
		for id, s := range cfg.servers {
			if cfg.contains(s) {
				continue
			}
			// Not part of configuration.
			if err := s.Close(); err != nil {
				debug.Println(err)
			}
			delete(cfg.servers, id)
		}
	}
}

func (cfg *configMgr) load(conf *Configuration) {
	cfg.oConf = conf.GetOldServers()
	cfg.nConf = conf.GetNewServers()

	if len(cfg.nConf) == 0 {
		cfg.setState(STABLE)
	} else {
		cfg.setState(JOINT)
	}

	// Clear membership of all servers.
	for _, s := range cfg.servers {
		s.SetɛCnil()
	}

	// Add new servers, if any and set membership.
	for _, id := range cfg.oConf {
		s := cfg.addServer(id, nil)
		s.SetɛCold()
	}
	for _, id := range cfg.nConf {
		s := cfg.addServer(id, nil)
		s.SetɛCnew()
	}
}

func (cfg *configMgr) getIndex(idx uint64) uint64 {
	if len(cfg.log) == 0 {
		return cfg.snapIdx
	}

	n := sort.Search(len(cfg.log), func(i int) bool { return cfg.log[i] >= idx })

	if n < len(cfg.log) && cfg.log[n] != idx {
		if n == 0 {
			idx = cfg.snapIdx
		} else {
			idx = cfg.log[n-1]
		}
	} else {
		idx = cfg.log[len(cfg.log)-1]
	}

	return idx
}

func (cfg *configMgr) set(idx uint64, conf *Configuration) bool {
	var curIdx uint64
	if len(cfg.log) != 0 {
		curIdx = cfg.log[len(cfg.log)-1]
	}

	// It is not ok to reload previous configurations using Set().
	if idx <= curIdx {
		return false
	} else if idx > curIdx {
		cfg.log = append(cfg.log, idx)
		cfg.load(conf)
	}

	return true
}

func (cfg *configMgr) setState(s ConfigState) {
	atomic.StoreUint32((*uint32)(&cfg.state), uint32(s))
}

func (cfg *configMgr) GetState() ConfigState {
	return ConfigState(atomic.LoadUint32((*uint32)(&cfg.state)))
}

func (cfg *configMgr) GetCurrentConfig() *Configuration {
	if len(cfg.log) == 0 {
		return cfg.snapConfig
	}

	var cmd Configuration
	cmd.OldServers = append(cmd.OldServers, cfg.oConf...)
	cmd.NewServers = append(cmd.NewServers, cfg.nConf...)

	return &cmd
}

func (cfg *configMgr) AddServer(uid string, t Transporter) server {
	cfg.Lock()
	defer cfg.Unlock()

	return cfg.addServer(uid, t)
}

func (cfg *configMgr) Contains(s server) bool {
	cfg.Lock()
	defer cfg.Unlock()

	return cfg.contains(s)
}

func (cfg *configMgr) GCServers() {
	cfg.Lock()
	defer cfg.Unlock()

	cfg.cleanUp()
}

/*
We can use binary search for truncation because cfg.log is sorted in ascending order.
As the only other way the cfg.log is modified is by Add() which only appends higher indices.
*/

func (cfg *configMgr) TruncateFrom(idx uint64) (err error) {
	cfg.Lock()
	defer cfg.Unlock()

	if len(cfg.log) == 0 {
		return
	}

	p := len(cfg.log) - 1
	n := sort.Search(len(cfg.log), func(i int) bool { return cfg.log[i] >= idx })
	cfg.log = cfg.log[:n]

	// Rollback.
	if n <= p {
		if len(cfg.log) == 0 {
			debug.Println("No configurations present in the log")
			cfg.setState(NILL)
			return
		}

		cIdx := cfg.log[n-1]

		var conf *Configuration
		var e *Entry
		if e, err = cfg.host.EntryAt(cIdx); err == nil {
			var cmd Command
			if cmd, err = e.command(); err != nil || cmd == nil {
				return
			}

			var ok bool
			conf, ok = cmd.(*Configuration)
			if !ok {
				err = fmt.Errorf("bug!! Recorded Configuration At %d; but read Non-configuration Entry from log\n", len(cfg.log)-1)
				return
			}
		} else if err == ErrSnapRd {
			conf = cfg.snapConfig
		}

		// Now load the configuration.
		cfg.load(conf)
	}

	return
}

func (cfg *configMgr) TruncateUpto(idx uint64) (err error) {
	cfg.Lock()
	defer cfg.Unlock()

	n := sort.Search(len(cfg.log), func(i int) bool { return cfg.log[i] >= idx })
	if n >= len(cfg.log) {
		cfg.log = cfg.log[:]
	} else {
		cfg.log = cfg.log[n:]
	}

	return
}

func (cfg *configMgr) SetSnapshot(idx uint64, conf *Configuration) {
	cfg.Lock()
	defer cfg.Unlock()

	cfg.snapIdx = idx
	cfg.snapConfig = conf
}

func (cfg *configMgr) Set(idx uint64, conf *Configuration) bool {
	cfg.Lock()
	defer cfg.Unlock()

	var curIdx uint64
	if len(cfg.log) != 0 {
		curIdx = cfg.log[len(cfg.log)-1]
	}

	// It is not ok to reload previous configurations using Set().
	if idx <= curIdx {
		return false
	} else if idx > curIdx {
		cfg.log = append(cfg.log, idx)
		cfg.load(conf)
	}

	return true
}

func (cfg *configMgr) GetIndex(idx uint64) uint64 {
	cfg.Lock()
	defer cfg.Unlock()

	return cfg.getIndex(idx)
}

func (cfg *configMgr) Get(idx uint64) (conf *Configuration) {
	cfg.Lock()
	defer cfg.Unlock()

	cIdx := cfg.getIndex(idx)
	if cIdx == cfg.snapIdx {
		return cfg.snapConfig
	}

	e, err := cfg.host.EntryAt(cIdx)
	if err != nil {
		debug.Println(err)
		return
	}
	cmd, err := e.command()
	if err != nil {
		debug.Println(err)
		return
	}

	conf, ok := cmd.(*Configuration)
	if !ok {
		debug.Printf("Impossible!! Recorded Configuration At %d; but read No-configuration Entry from log\n", cIdx)
	}
	return
}

func (cfg *configMgr) HasMajority(f func(s server) bool) bool {
	cfg.Lock()
	defer cfg.Unlock()

	var ok bool
	ovotes, nvotes := 0, 0

	switch cfg.GetState() {
	case JOINT:
		for _, s := range cfg.servers {
			if s.ɛCold() && f(s) {
				ovotes++
			}
			// Not else if. server can be part of both Cold and Cnew.
			if s.ɛCnew() && f(s) {
				nvotes++
			}
		}

		ok = ovotes >= len(cfg.oConf)/2+1 && nvotes >= len(cfg.nConf)/2+1

	case STABLE:
		for _, s := range cfg.servers {
			if s.ɛCold() && f(s) {
				ovotes++
			}
		}

		ok = ovotes >= len(cfg.oConf)/2+1
	}

	return ok
}

func (cfg *configMgr) GetMajority(f func(s server) comparable) comparable {
	cfg.Lock()
	defer cfg.Unlock()

	var maj comparable
	cfg.ovals = cfg.ovals[:0]

	switch cfg.GetState() {
	case JOINT:
		cfg.nvals = cfg.nvals[:0]

		for _, s := range cfg.servers {
			if s.ɛCold() {
				cfg.ovals = append(cfg.ovals, f(s))
			}
			// Not else if. server can be part of both Cold and Cnew.
			if s.ɛCnew() {
				cfg.nvals = append(cfg.nvals, f(s))
			}
		}
		if len(cfg.ovals) != len(cfg.oConf) || len(cfg.nvals) != len(cfg.nConf) {
			debug.Println("Bug!! list of servers OR their membership is inconsistent")
		}

		sort.Sort(sort.Reverse(comparableSlice(cfg.ovals)))
		sort.Sort(sort.Reverse(comparableSlice(cfg.nvals)))

		omaj := cfg.ovals[len(cfg.ovals)/2]
		nmaj := cfg.nvals[len(cfg.nvals)/2]
		if omaj.Less(nmaj) {
			maj = omaj
		} else {
			maj = nmaj
		}

	case STABLE:
		for _, s := range cfg.servers {
			if s.ɛCold() {
				cfg.ovals = append(cfg.ovals, f(s))
			}
		}
		sort.Sort(sort.Reverse(comparableSlice(cfg.ovals)))
		maj = cfg.ovals[len(cfg.ovals)/2]
	}

	return maj
}

func (cfg *configMgr) Invoke(uid string, f func(s server)) {
	cfg.Lock()
	defer cfg.Unlock()

	if s, ok := cfg.servers[uid]; ok {
		f(s)
	}
}

func (cfg *configMgr) InvokeAll(par bool, f func(s server)) {
	cfg.Lock()
	defer cfg.Unlock()

	if par {
		var sg sync.WaitGroup
		sg.Add(len(cfg.servers))
		for _, s := range cfg.servers {
			s := s
			go func() {
				defer sg.Done()
				f(s)
			}()
		}
		sg.Wait()
	} else {
		for _, s := range cfg.servers {
			f(s)
		}
	}
}
