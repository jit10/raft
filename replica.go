package raft

import (
	"encoding/binary"
	"fmt"
	"io"
	"math/rand"
	"sort"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"
)

const (
	commandQSize   = (1 << 8)
	reqQSize       = (1 << 4)
	pipelineSize   = (1 << 4)
	heartbeatRatio = 5
)

// Raft Server states.
const (
	Leader = iota
	Follower
	Candidate
	PreCandidate

	nStates
)

var stateString = [...]string{"Leader", "Follower", "Candidate", "PreCandidate"}

// Following are possible errors when called ClientResponse.Response.Error().
var (
	ErrUnkCmd    = fmt.Errorf("unknown Raft Command")
	ErrNotLeader = fmt.Errorf("server is not Raft leader")
	ErrLog       = fmt.Errorf("logging Failed")
	ErrCmdCap    = fmt.Errorf("too many pending requests")
	ErrReconf    = fmt.Errorf("can't accept new Reconfiguration request while pending requests")
	ErrBadConf   = fmt.Errorf("bad Configuration; majority of servers in staged configuration are not making progress")
	ErrTmOut     = fmt.Errorf("timed out while processing request")
	ErrSM        = fmt.Errorf("nil StateMachine")
	ErrCmd       = fmt.Errorf("nil ClientCommand")
	ErrBootstrap = fmt.Errorf("can't bootstrap with existing entries in the log")
)

func init() {
	if err := RegisterCommands(&NOPCommand{}, &AddServerCommand{}, &DelServerCommand{}, &ReconfigureCommand{}, &Configuration{}); err != nil {
		debug.Println(err)
	}
}

func min(x, y uint64) uint64 {
	if x < y {
		return x
	}
	return y
}

func max(x, y uint64) uint64 {
	if x > y {
		return x
	}
	return y
}

type errResp struct {
	error
}

func (resp errResp) Error() error {
	return resp.error
}

// ClientResponse is the response received from call to Server.Issue.
type ClientResponse struct {
	leaderID string
	Response
}

// LeaderID returns the leaderID or empty string received as a response from server.
func (clresp *ClientResponse) LeaderID() string {
	return clresp.leaderID
}

func mkClientResponse(leaderID string, resp Response) *ClientResponse {
	return &ClientResponse{
		leaderID: leaderID,
		Response: resp,
	}
}

type clientMsg struct {
	clcmd *ClientCommand
	respC chan *ClientResponse
}

type rpcMsg struct {
	RPC
	*peer
}

type respChs struct {
	start uint64
	end   uint64

	resps map[uint64]chan *ClientResponse
}

func (chs *respChs) Add(idx uint64, respC chan *ClientResponse) {
	if len(chs.resps) == 0 {
		chs.start = idx
	} else if idx <= chs.end {
		debug.Printf("Bug!!! log index must grow monotonically increasing")
	}

	chs.end = idx
	if respC != nil {
		chs.resps[idx] = respC
	}
}

func (chs *respChs) Remove(idx uint64) (respC chan *ClientResponse) {
	var ok bool
	if respC, ok = chs.resps[idx]; ok {
		if idx < chs.start {
			debug.Printf("Bug!!! Execute should follow log order")
			return
		}
		delete(chs.resps, idx)

		var i uint64
		for i = idx; i <= chs.end; i++ {
			if _, ok := chs.resps[i]; ok {
				break
			}
		}
		chs.start = i
	}

	return
}

func (chs *respChs) TruncateFrom(idx uint64) {
	if chs.end == 0 {
		return
	}
	var i uint64
	for i = chs.end; i >= idx && i >= chs.start; i-- {
		if respC, ok := chs.resps[i]; ok {
			respC <- mkClientResponse("", errResp{ErrNotLeader})
			delete(chs.resps, i)
		}
	}
	for _, ok := chs.resps[i]; !ok && i >= chs.start; i-- {
	}
	chs.end = i
}

func (chs *respChs) TruncateUpto(idx uint64) {
	if chs.start == 1 {
		return
	}

	var i uint64
	for i = chs.start; i < idx && i <= chs.end; i++ {
		if respC, ok := chs.resps[i]; ok {
			respC <- mkClientResponse("", errResp{ErrNotLeader})
			delete(chs.resps, i)
		}
	}
	for _, ok := chs.resps[i]; !ok && i <= chs.end; i++ {
	}
	chs.start = i
}

// State represents raft Server state.
type State uint32

func (s State) String() string {
	if s < nStates {
		return stateString[s]
	}
	return fmt.Sprintf("Invalid")
}

// Server represents a raft replica.
type Server interface {
	// GetState returns the currents state.
	GetState() State
	// LeaderID returns the Leader ID if known.
	LeaderID() string

	// GetConfiguration returns the configuration state and the current configuration in use.
	GetConfiguration() (ConfigState, *Configuration)

	// SetElectionTimeout changes the election timeout.
	SetElectionTimeout(time.Duration) error
	// GetElectionTimeout retrieves the election timeout.
	GetElectionTimeout() time.Duration

	// CanBootstrap returns if the server could be bootstrapped.
	CanBootstrap() bool
	// BootStrap bootstraps the raft server as leader, if possible.
	Bootstrap() error

	// F = {Start(), Stop()}. ∀f1∈F,∀f2∈F, (f1 ∦ f2)
	// Start starts the Server. should be started before issuing commands/changing configurations.
	Start() error
	// Stop stops the server gracefully.
	Stop() error

	// LogChecksum returns checksum of its log.
	LogChecksum() string

	// Issue issues a ClientCommand. Returns ErrCmdCap for failure or nil if succeeds.
	// It is guaranteed that a response will eventually be received from the channel(if majority of servers are up).
	Issue(*ClientCommand) (<-chan *ClientResponse, error)

	// Add tries to Add a new server within a given deadline. nil return represents error.
	Add(uid string, timeout time.Duration) *ClientResponse
	// Remove tries to Remove a new server within a given deadline. nil return represents error.
	Remove(uid string, timeout time.Duration) *ClientResponse
	// Reconfigure tries to reconfigure the cluster of replicas to a different cluster within a given deadline. nil return represents error.
	Reconfigure(srvs []string, timeout time.Duration) *ClientResponse
}

// Satisfies Server interface.
type replica struct {
	running bool

	wg sync.WaitGroup

	State

	commitIdx   uint64
	lastApplied uint64

	self string

	// Membership.
	ɛ

	// leader peer. Only valid for Follower.
	leader unsafe.Pointer

	commandC chan clientMsg
	msgC     chan rpcMsg

	// staged configuration.
	staging bool
	sConf   []string
	// progress of peers.
	pindices []uint64

	// configuration message from client currently being processed. Only valid for Leader.
	cfgMsg clientMsg

	// Client response channels.
	*respChs

	// Snapshotter to dump ongoing snapshot requests. Only valid for Follower.
	Snapshotter

	*ConfigParams
	configurator
	Log
	Communication
	StateMachine

	// functions to get fresh instances of Communication and Log.
	commFn func(string) (Communication, error)
	logFn  func(string, uint32) (Log, error)

	// Global shutdown event.
	shutdownC chan bool
}

/* server implementation */

func (r *replica) Send(rpc RPC) bool {
	return true
}

func (r *replica) AppendEntries(force bool) bool {
	return true
}

func (r *replica) RequestVote(actual bool) bool {
	return true
}

func (r *replica) UID() string {
	return r.self
}

func (r *replica) MatchIndex() uint64 {
	return r.Log.LastIndex()
}

// replica always votes for itself.
func (r *replica) VoteGranted() bool {
	return true
}

func (r *replica) ResetState() {
}

func (r *replica) Reset(t Transporter) error {
	return nil
}

func (r *replica) Close() error {
	return nil
}

/* ... */

// NewServer creates a new raft replica.
func NewServer(args *ConfigParams, sm StateMachine, newComm func(uid string) (Communication, error), openLog func(path string, maxEntries uint32) (Log, error)) (s Server, err error) {
	if sm == nil {
		err = ErrSM
		return
	}

	r := &replica{
		State:        Follower,
		self:         args.ServerID,
		ConfigParams: args,
		StateMachine: sm,
	}

	if newComm == nil {
		newComm = newTcpComm
	}
	r.commFn = newComm
	if r.Communication, err = r.commFn(args.ServerID); err != nil {
		return
	}

	if openLog == nil {
		openLog = openDefaultLog
	}
	r.logFn = openLog
	if r.Log, err = r.logFn(args.LogPath, args.MaxEntries); err != nil {
		return
	}

	r.configurator = &configMgr{
		servers: make(map[string]server),
		host:    r,
	}

	// Add all configurations in the log.
	for i := r.Log.FirstIndex(); i > 0 && i <= r.Log.LastIndex(); i++ {
		var cmd Command
		var e *Entry
		if e, err = r.Log.EntryAt(i); err != nil {
			return
		}

		if cmd, err = e.command(); err != nil {
			return
		}
		conf, ok := cmd.(*Configuration)
		if ok {
			r.configurator.Set(i, conf)
		}
	}
	r.configurator.GCServers()

	// Recover from snapshot.
	if err = r.readSnapshot(); err != nil {
		return
	}

	r.commandC = make(chan clientMsg, commandQSize)
	r.msgC = make(chan rpcMsg, reqQSize)

	r.respChs = &respChs{
		resps: make(map[uint64]chan *ClientResponse),
	}

	r.shutdownC = make(chan bool)

	s = r
	return
}

/* Server Implementation */

func (r *replica) GetState() State {
	return State(atomic.LoadUint32((*uint32)(&r.State)))
}

func (r *replica) GetConfiguration() (ConfigState, *Configuration) {
	return r.configurator.GetState(), r.configurator.GetCurrentConfig()
}

func (r *replica) SetElectionTimeout(t time.Duration) (err error) {
	u := t / r.baseTime()
	if u < heartbeatRatio {
		err = fmt.Errorf("election time must be more than %+v", time.Duration(heartbeatRatio)*r.baseTime())
		return
	}
	atomic.StoreUint32(&r.ConfigParams.ElectTmUnits, uint32(u))
	return
}

func (r *replica) GetElectionTimeout() time.Duration {
	return time.Duration(atomic.LoadUint32(&r.ConfigParams.ElectTmUnits)) * r.baseTime()
}

func (r *replica) Issue(clcmd *ClientCommand) (respC <-chan *ClientResponse, err error) {
	if clcmd == nil || clcmd.cmd == nil {
		err = ErrCmd
	}

	rC := make(chan *ClientResponse, 1)
	// Send the command.
	select {
	case r.commandC <- clientMsg{clcmd, rC}:
		respC = rC
	default:
		err = ErrCmdCap
	}

	return
}

func (r *replica) Add(uid string, timeout time.Duration) (resp *ClientResponse) {
	respC, err := r.Issue(MkClientCommand(0, r.self, &AddServerCommand{ServerId: uid}))
	if err != nil {
		return
	}

	select {
	case resp = <-respC:
	case <-time.After(timeout):
		resp = mkClientResponse("", errResp{ErrTmOut})
	}

	return
}

func (r *replica) Remove(uid string, timeout time.Duration) (resp *ClientResponse) {
	respC, err := r.Issue(MkClientCommand(0, r.self, &DelServerCommand{ServerId: uid}))
	if err != nil {
		return
	}

	select {
	case resp = <-respC:
	case <-time.After(timeout):
		resp = mkClientResponse("", errResp{ErrTmOut})
	}

	return
}

func (r *replica) Reconfigure(srvs []string, timeout time.Duration) (resp *ClientResponse) {
	respC, err := r.Issue(MkClientCommand(0, r.self, &ReconfigureCommand{Servers: srvs}))
	if err != nil {
		return
	}

	select {
	case resp = <-respC:
	case <-time.After(timeout):
		resp = mkClientResponse("", errResp{ErrTmOut})
	}

	return
}

func (r *replica) CanBootstrap() bool {
	return r.Log.LastIndex() == 0 && r.Log.CurrentTerm() == 0
}

func (r *replica) Bootstrap() (err error) {
	if r.Log.LastIndex() != 0 {
		err = ErrBootstrap
		return
	}

	r.setState(Leader)
	if err = r.Log.UpdateCurrentTerm(1); err != nil {
		return
	}

	conf := &Configuration{
		OldServers: []string{r.self},
	}
	clcmd := MkClientCommand(0, r.self, conf)
	if _, err = r.writeCommand(clcmd, nil); err == nil {
		err = r.Log.FlushEntries()
	}

	if err == nil {
		r.configurator.Set(1, conf)
		r.commitIdx = 1
	}

	return
}

func (r *replica) Start() (err error) {
	if r.running {
		return nil
	}
	r.running = true

	select {
	case <-r.shutdownC:
		if r.Communication, err = r.commFn(r.ConfigParams.ServerID); err == nil {
			r.Log, err = r.logFn(r.ConfigParams.LogPath, r.ConfigParams.MaxEntries)
		}
		if err != nil {
			return
		}

		r.shutdownC = make(chan bool)

	default:
	}

	r.wg.Add(1)
	go func() {
		defer func() {
			r.Communication.Close()
			r.wg.Done()
		}()

		var err error
		for {
			var t Transporter
			t, err = r.Communication.Accept()
			if err == nil {
				r.configurator.AddServer(t.RemoteUID(), t)
			} else {
				debug.Println("Listener Accept failed:", err)
				break
			}
		}
	}()

	r.wg.Add(1)
	go func() {
		defer r.wg.Done()
		r.process()
	}()

	return
}

func (r *replica) Stop() error {
	r.running = false

	r.Communication.Close()

	// Make sure it is not already closed(Multiple calls to Stop()).
	select {
	case <-r.shutdownC:
		return nil
	default:
		close(r.shutdownC)
	}

	r.wg.Wait()

	r.configurator.InvokeAll(true, func(s server) { s.Close() })
	return r.Log.Close()
}

func (r *replica) LogChecksum() string {
	return r.Log.Checksum()
}

// May not be accurate. But again it's not supposed to be.
func (r *replica) LeaderID() string {
	if r.GetState() == Leader {
		return r.self
	}
	return r.getLeader()
}

/* ... */

func (r *replica) msgQ() chan<- rpcMsg {
	return r.msgC
}

func (r *replica) commitIndex() uint64 {
	return r.commitIdx
}

func (r *replica) maxEntriesPerRequest() int {
	return r.ConfigParams.BatchLimit
}

func (r *replica) maxRequestSize() int {
	return r.ConfigParams.MaxReqSize
}

func (r *replica) baseTime() time.Duration {
	return time.Duration(r.ConfigParams.BaseTimeUnit) * time.Microsecond
}

func (r *replica) heartbeatUnits() uint32 {
	// TODO: this could be adapted based on network latency and electeTmUnits.
	return r.ConfigParams.ElectTmUnits / heartbeatRatio
}

func (r *replica) getLeader() string {
	p := (*peer)(atomic.LoadPointer(&r.leader))
	if p == nil {
		return ""
	}
	return p.UID()
}

func (r *replica) updateLeader(p *peer) {
	atomic.StorePointer(&r.leader, unsafe.Pointer(p))
}

// New Peer Connection.
func (r *replica) addPeer(uid string, t Transporter) server {
	// replica is peer of itself.
	if uid == r.self {
		return r
	}

	p := &peer{
		uid:         uid,
		Transporter: t,
		rpcC:        make(chan RPC, pipelineSize),
		quitC:       make(chan bool),
		dialC:       make(chan bool, 1),
		æρℓ:         make(chan uint64, pipelineSize),
		AEUnits:     1,
		AETm:        time.NewTimer(r.baseTime()),
		host:        r,
	}

	p.dialC <- true

	p.SetɛCnil()

	if t != nil {
		p.alive = true
		p.Start()
	}

	// Init default state.
	p.ResetState()

	return p
}

func (r *replica) setState(state State) {
	atomic.StoreUint32((*uint32)(&r.State), uint32(state))
}

func (r *replica) electionTimeout() time.Duration {
	t := r.ConfigParams.ElectTmUnits
	t += uint32(rand.Int31n(int32(t)))
	return time.Duration(t*r.ConfigParams.BaseTimeUnit) * time.Microsecond
}

func (r *replica) commandTimeout() time.Duration {
	// TODO: Should be << electTmLb and should adapt based on some factors(latency, disk write speed..)
	return r.baseTime()
}

func when(b bool, c <-chan time.Time) <-chan time.Time {
	if b {
		return c
	}
	return nil
}

func (r *replica) process() {
	var err error

	cmdTm := time.NewTimer(r.commandTimeout())
	electTm := time.NewTimer(r.electionTimeout())
	confTm := time.NewTimer(r.GetElectionTimeout())
	gctick := time.NewTicker(5 * time.Second)

	// If it is just bootstrapped server, make it leader.
	if r.configurator.HasMajority(func(s server) bool { return s.VoteGranted() }) {
		r.announceLeader()
	}

	for {
		state := r.GetState()
		isLeader := state == Leader
		isMember := !r.ɛCnil()

		select {
		// shouldn't GC while staging.
		case <-when(isMember && isLeader && !r.staging, gctick.C):
			r.configurator.GCServers()

		case <-when(isMember && !isLeader, electTm.C):
			r.setState(PreCandidate)
			err = r.bcastPreRequestVote()
			electTm.Reset(r.electionTimeout())

		case <-when(isLeader, cmdTm.C):
			err = r.bcastAppendEntries()
			cmdTm.Reset(r.commandTimeout())

		// Transition to Joint Consensus state or send failure.
		case <-when(isLeader && r.staging, confTm.C):
			err = r.writeStagedConfig()
			confTm.Reset(r.GetElectionTimeout())

		case msg := <-r.msgC:
			switch msg.RPC.Kind() {
			case AppendReqRPC:
				err = r.processAppendEntriesRequest(msg)

				// Someone sent me AERequest. So there may be a leader. Reset the election timer.
				if state == Follower && !electTm.Reset(r.electionTimeout()) {
					<-electTm.C
				}

			case AppendRespRPC:
				err = r.processAppendEntriesResponse(msg)

			case VoteReqRPC:
				err = r.processVoteRequest(msg)

			case VoteRespRPC:
				err = r.processVoteResponse(msg)

				// PreCandidate -> Candidate transition => Reset election timer.
				if state == PreCandidate && r.GetState() == Candidate && !electTm.Reset(r.electionTimeout()) {
					<-electTm.C
				}

			case SnapReqRPC:
				err = r.processSnapRequest(msg)

			case SnapRespRPC:
				err = r.processSnapResponse(msg)
			}

		case cmsg := <-r.commandC:
			if cmsg.clcmd.isRaftCommand() {
				err = r.processRaftCommand(cmsg)
			} else {
				err = r.processClientCommand(cmsg)
			}

		case <-r.shutdownC:
			//stop timers and break out of loop.
			confTm.Stop()
			cmdTm.Stop()
			electTm.Stop()
			goto processLoopExit
		}

		// panic(after closing fds) if any error occured in processing.
		if err != nil {
			r.Log.Close()
			r.configurator.InvokeAll(true, func(s server) { s.Close() })
			panic(err)
		}
	}
processLoopExit:
}

func (r *replica) vfySnapshot(sr Snapshotter) (s *RaftState, rd io.Reader, err error) {
	if sr == nil {
		return
	}

	rd = sr.Reader()
	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]
	k, _ := rd.Read(bs)

	var blen uint64
	if k > 0 {
		blen, k = binary.Uvarint(bs[:k])
	}
	if k <= 0 {
		err = fmt.Errorf("failed to read length of RaftState")
		return
	}
	b := make([]byte, blen)

	if _, err = rd.Read(b); err != nil {
		return
	}

	var rs RaftState
	if err = rs.Unmarshal(b); err == nil {
		s = &rs
	}

	return
}

func (r *replica) writeSnapshot() (sr Snapshotter, err error) {
	conf := r.configurator.Get(r.commitIdx)
	if conf == nil {
		err = fmt.Errorf("bug!! Configurator must have the latest commited configuration")
		return
	}

	s := &RaftState{
		SnapIdx:       r.commitIdx,
		SnapTerm:      r.Log.TermAt(r.commitIdx),
		Configuration: conf,
	}

	if sr, err = r.Log.NewSnapshotter(); err != nil {
		return
	}

	b, err := s.Marshal()
	if err == nil {
		var ba [binary.MaxVarintLen64]byte
		bs := ba[:]
		binary.PutUvarint(bs, uint64(len(b)))
		if _, err = sr.Write(bs); err == nil {
			_, err = sr.Write(b)
		}
	}

	if err != nil {
		return
	}

	if err = r.StateMachine.Snapshot(sr.(io.Writer)); err != nil {
		sr.Close()
		err = sr.Remove()
	}

	return
}

func (r *replica) writeEntry(e *Entry) (idx uint64, err error) {
	if idx, err = r.Log.WriteEntry(e); err != nil && err == ErrSnapWr {
		var sr Snapshotter
		if sr, err = r.writeSnapshot(); err == nil {
			err = r.Log.SetSnapshotter(sr)
		}
		if err != nil {
			return
		}

		// Keep snapThresold entries even after snapshot; most slow peers could update without sending snapshot.
		if err = r.Log.TruncateUpto(r.commitIdx - uint64(r.ConfigParams.SnapThresold) + 1); err != nil {
			return
		}
		r.respChs.TruncateUpto(r.commitIdx + 1)
		r.configurator.TruncateUpto(r.commitIdx + 1)
	}

	return
}

func (r *replica) writeCommand(clcmd *ClientCommand, respC chan *ClientResponse) (idx uint64, err error) {
	e, err := mkEntry(r.Log.CurrentTerm(), clcmd)
	if err != nil {
		return
	}

	if idx, err = r.writeEntry(e); err != nil {
		return
	}

	r.respChs.Add(idx, respC)
	return
}

func (r *replica) writeReply(idx uint64, resp *ClientResponse) {
	respC := r.respChs.Remove(idx)
	if respC != nil {
		respC <- resp
	}
}

func (r *replica) processClientCommand(cmsg clientMsg) (err error) {
	// Only leader logs command.
	if r.GetState() != Leader {
		cmsg.respC <- mkClientResponse(r.LeaderID(), errResp{ErrNotLeader})
		return
	}

	if _, err = r.writeCommand(cmsg.clcmd, cmsg.respC); err != nil {
		cmsg.respC <- mkClientResponse("", errResp{ErrLog})
	}

	return
}

func (r *replica) processRaftCommand(cmsg clientMsg) (err error) {
	clcmd := cmsg.clcmd

	// Only leader logs commands.
	if r.GetState() != Leader {
		cmsg.respC <- mkClientResponse(r.LeaderID(), errResp{ErrNotLeader})
		return
	}

	// Already have a configuration in progress.
	if r.configurator.GetState() != STABLE || r.commitIdx < r.configurator.GetIndex(r.Log.LastIndex()) {
		cmsg.respC <- mkClientResponse("", errResp{ErrReconf})
		return
	}

	cmd := clcmd.cmd
	oconf := r.configurator.GetCurrentConfig().GetOldServers()
	switch cmd.(type) {
	case *AddServerCommand:
		addID := cmd.(*AddServerCommand).GetServerId()
		for _, s := range oconf {
			if s == addID {
				// Already exists. Reply success.
				cmsg.respC <- mkClientResponse("", errResp{nil})
				return
			}
		}
		r.sConf = append(r.configurator.GetCurrentConfig().GetOldServers(), addID)

	case *DelServerCommand:
		delID := cmd.(*DelServerCommand).GetServerId()
		for _, s := range oconf {
			if s != delID {
				r.sConf = append(oconf, s)
			}
		}
		if len(oconf) == len(r.sConf) {
			// Doesn't exist. Reply success.
			cmsg.respC <- mkClientResponse("", errResp{nil})
			return
		}

	case *ReconfigureCommand:
		nconf := cmd.(*ReconfigureCommand).GetServers()
		// Check if I am already using the proposed Configuration.
		if len(oconf) == len(nconf) {
			sort.Strings(oconf)
			sort.Strings(nconf)
			var i int
			for i = range oconf {
				if oconf[i] != nconf[i] {
					break
				}
			}
			if i == len(oconf) {
				cmsg.respC <- mkClientResponse("", errResp{nil})
				return
			}
		}
		r.sConf = nconf

	default:
		debug.Println(ErrUnkCmd)
		cmsg.respC <- mkClientResponse("", errResp{ErrUnkCmd})
		return
	}

	r.staging = true
	r.cfgMsg = cmsg
	r.pindices = r.pindices[:0]
	for _, id := range r.sConf {
		r.pindices = append(r.pindices, r.configurator.AddServer(id, nil).MatchIndex())
	}

	return
}

func (r *replica) writeStagedConfig() (err error) {
	var cb, cg int
	var f func(s server)

	rIdx := r.Log.LastIndex()
	for i, pid := range r.sConf {
		f = func(s server) {
			nIdx := s.MatchIndex()

			// has caught up with in thresold? Return.
			if nIdx+uint64(r.maxEntriesPerRequest()) >= rIdx {
				cg++
				return
			}

			// Else check if it is greater than last recorded index.
			if nIdx > r.pindices[i] {
				r.pindices[i] = nIdx
			} else {
				cb++
			}
		}

		r.configurator.Invoke(pid, f)
	}

	cmsg := r.cfgMsg
	n := len(r.sConf)
	if cb >= n-n/2 {
		debug.Println(ErrBadConf)
		r.configurator.GCServers()
		cmsg.respC <- mkClientResponse("", errResp{ErrBadConf})
		r.cfgMsg = clientMsg{}
		r.staging = false
	} else if cg >= n/2+1 {
		debug.Println("Initiating new configuration", r.sConf)
		conf := &Configuration{
			OldServers: r.configurator.GetCurrentConfig().GetOldServers(),
			NewServers: r.sConf,
		}
		cmsg.clcmd = MkClientCommand(cmsg.clcmd.seq, cmsg.clcmd.clientID, conf)
		r.cfgMsg = cmsg

		var idx uint64
		if idx, err = r.writeCommand(cmsg.clcmd, nil); err != nil {
			cmsg.respC <- mkClientResponse("", errResp{ErrLog})
			r.cfgMsg = clientMsg{}
			return
		}
		if !r.Set(idx, conf) {
			debug.Println("Failed to set Command")
		}
		r.staging = false
	}

	return
}

func (r *replica) bcastPreRequestVote() (err error) {
	if err = r.Log.FlushEntries(); err != nil {
		return
	}
	r.configurator.InvokeAll(true, func(s server) { s.RequestVote(false) })

	return
}

func (r *replica) bcastRequestVote() (err error) {
	// Increment Term
	if err = r.Log.UpdateCurrentTerm(r.CurrentTerm() + 1); err == nil {
		// So that LastIndex() may update.
		err = r.Log.FlushEntries()
	}
	if err != nil {
		return
	}

	// Vote for self.
	if err = r.Log.UpdateVotedFor(r.UID()); err != nil {
		return
	}
	debug.Printf("%s Vote granted to %s for Term %d\n", r.self, r.self, r.Log.CurrentTerm())

	// Empty the leader.
	r.updateLeader(nil)

	r.discardSnapshotter()

	r.configurator.InvokeAll(true, func(s server) { s.RequestVote(true) })

	return
}

func (r *replica) bcastAppendEntries() (err error) {
	if err = r.FlushEntries(); err != nil {
		return
	}
	r.configurator.InvokeAll(true, func(s server) { s.AppendEntries(false) })
	return
}

func (r *replica) announceLeader() {
	debug.Printf("%s Leader for Term %d\n", r.self, r.Log.CurrentTerm())

	r.setState(Leader)
	// Reset nextIndex and matchIndex of all servers and force send AE request.
	r.configurator.InvokeAll(true, func(s server) { s.ResetState(); s.AppendEntries(true) })
}

func (r *replica) discardSnapshotter() {
	// Close and remove any partial snapshot.
	if r.Snapshotter != nil {
		err := r.Snapshotter.Close()
		if err == nil {
			err = r.Snapshotter.Remove()
		}
		if err != nil {
			debug.Println(err)
		}
		r.Snapshotter = nil
	}
}

// Tries to stepDown from Leader to Follower if term > CurrentTerm.
// Returns ok = true, if it did step down.
func (r *replica) stepDown(term uint64) (ok bool, err error) {
	if term > r.Log.CurrentTerm() {
		ok = true

		// state = Follower.
		r.setState(Follower)

		// Set staging to false.
		r.staging = false

		r.discardSnapshotter()

		// Update term and set voteId to nil.
		if err = r.Log.UpdateCurrentTerm(term); err == nil {
			err = r.Log.UpdateVotedFor("")
		}
	}

	return
}

func (r *replica) processVoteRequest(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	req, ok := rpc.(*RequestVoteRequest)
	if !ok {
		debug.Println("Recieved Invalid VoteReqRPC")
		return
	}

	// Change to follower and update term if found a higher term.
	ok, err = r.stepDown(req.Term)
	if err != nil {
		return
	}

	if req.Term < r.Log.CurrentTerm() {
		debug.Printf("%s Received Old vote Request %v < %v\n", r.self, req.Term, r.Log.CurrentTerm())

		p.Send(mkRequestVoteResponse(r.Log.CurrentTerm(), false, req.Pre))
		return
	}

	// So that LastIndex() may update.
	if err = r.Log.FlushEntries(); err != nil {
		return
	}

	voteID := r.Log.VotedFor()
	lastLogIdx := r.Log.LastIndex()
	lastLogTerm := r.Log.TermAt(lastLogIdx)

	if req.LastLogTerm > lastLogTerm || req.LastLogTerm == lastLogTerm && req.LastLogIdx >= lastLogIdx {
		var ok bool
		state := r.GetState()

		if !req.Pre {
			if voteID == "" || voteID == p.UID() {
				if err = r.Log.UpdateVotedFor(p.UID()); err != nil {
					return
				}
				ok = true

				debug.Printf("%s Vote granted to %s for Term %d\n", r.self, p.UID(), r.Log.CurrentTerm())
			} else {
				debug.Printf("%s Can't grant vote to %s for Term %d because already voted\n", r.self, p.UID(), r.Log.CurrentTerm())
			}
		} else {
			if state == PreCandidate || state == Candidate {
				ok = true

				debug.Printf("%s Pre-Vote granted to %s\n", r.self, p.UID())
			} else {
				debug.Printf("%s can't grant pre-vote to %s as it's election Timer has not expired", r.self, p.UID())
			}
		}

		p.Send(mkRequestVoteResponse(r.Log.CurrentTerm(), ok, req.Pre))
		return
	}

	debug.Printf("%s Can't grant vote to %s for Term %d because of log inconsistency\n", r.self, p.UID(), r.Log.CurrentTerm())

	p.Send(mkRequestVoteResponse(r.Log.CurrentTerm(), false, req.Pre))

	return
}

func (r *replica) processVoteResponse(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	resp, ok := rpc.(*RequestVoteResponse)
	if !ok {
		debug.Println("Recieved Invalid VoteRespRPC")
		return
	}

	// This response is received too late.
	state := r.GetState()
	if (state != Candidate || resp.Pre) && (state != PreCandidate || !resp.Pre) {
		return
	}

	// If response term is less than current term, then ignore and return.
	if resp.Term < r.Log.CurrentTerm() {
		debug.Println("Vote response received too late; election timer re-expired")
		return
	}

	// Change to follower and update term if found a higher term and return.
	ok, err = r.stepDown(resp.Term)
	if ok || err != nil {
		return
	}

	// Now term matches current term. But Vote was not granted. Return.
	if !resp.VoteGranted {
		return
	}

	// peer has granted vote and check for majority ⇒ leader and announce.
	p.GrantVote()
	if r.configurator.HasMajority(func(s server) bool { return s.VoteGranted() }) {
		if state == Candidate {
			r.announceLeader()
		} else {
			debug.Printf("%s now requesting vote\n", r.self)

			r.setState(Candidate)
			r.bcastRequestVote()
		}
	}
	return
}

// Cross reference this with peer processAppendEntriesResponse() function.
func (r *replica) processAppendEntriesRequest(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	req, ok := rpc.(*AppendEntriesRequest)
	if !ok {
		debug.Println("Recieved Invalid AppendReqRPC")
		return
	}

	if req.Term < r.Log.CurrentTerm() {
		debug.Printf("Old appendEntries Request %v < %v\n", req.Term, r.Log.CurrentTerm())

		// Index should be the index of the first entry in the request.
		p.Send(mkAppendEntriesResponse(r.Log.CurrentTerm(), req.PrevIdx+uint64(len(req.Entries)), false))
		return
	}

	// Change to follower and update term if found a higher term and continue processing.
	ok, err = r.stepDown(req.Term)
	if err != nil {
		return
	}

	// Now the terms match.
	// update leader. and state = Follower.
	r.updateLeader(p)
	r.setState(Follower)

	prevterm := r.Log.TermAt(req.PrevIdx)
	if prevterm != req.PrevTerm && req.PrevIdx >= r.Log.FirstIndex() {
		debug.Printf("Non - matching Term(got %d : has %d) at %d\n", req.Term, prevterm, req.PrevIdx)

		idx := req.PrevIdx + uint64(len(req.Entries))
		// If i am lagging send the last log index, else send the index of the last entry in the request.
		if prevterm == 0 {
			idx = r.Log.LastIndex()
		}
		p.Send(mkAppendEntriesResponse(r.Log.CurrentTerm(), idx, false))
		return
	}

	// Skip those entries which i already have and the corresponding terms match. (May happen for duplicate requests).
	idx := req.PrevIdx + 1
	for ; idx <= req.PrevIdx+uint64(len(req.Entries)) && idx <= r.Log.LastIndex(); idx++ {
		// If already snapshotted entry ignore.
		if idx < r.Log.FirstIndex() {
			continue
		} else if r.Log.TermAt(idx) != req.Entries[idx-req.PrevIdx-1].Term() {
			debug.Printf("Deleting all the entries from %d\n", idx)

			r.Log.TruncateFrom(idx)
			r.respChs.TruncateFrom(idx)

			// Also truncate configurations and rollback.
			r.configurator.TruncateFrom(idx)
			break
		}
	}

	// Append new entries, if any.
	for _, e := range req.Entries[idx-req.PrevIdx-1:] {
		var cIdx uint64
		cIdx, err = r.writeEntry(e)
		if err != nil {
			break
		}

		// Check for Configuration.
		if e.isRaftCommand() {
			var cmd Command
			if cmd, err = e.command(); err != nil {
				debug.Println(err)
				break
				// It is a Reconfiguration.
			} else if conf, ok := cmd.(*Configuration); ok {
				// Flush log now and set the new configuration.
				err = r.Log.FlushEntries()
				r.configurator.Set(cIdx, conf)
			}
		}
	}
	if err == nil {
		err = r.Log.FlushEntries()
	}

	// Logging failed.
	if err != nil {
		debug.Println("Logging failed")
		return
	}

	// Update commitIdx to the index sent by leader and execute any newly commited commands.
	if req.LeaderCommit > r.commitIdx {
		r.commitIdx = min(req.LeaderCommit, r.Log.LastIndex())
		err = r.exec()
	}

	// Reply success with Index = last entry's in this request.
	// If Leader has not commited PrevIdx, respond to heartbeat messages.(Tells the leader that this follower has upto PrevIdx)
	if len(req.Entries) == 0 && req.LeaderCommit < req.PrevIdx || len(req.Entries) != 0 {
		p.Send(mkAppendEntriesResponse(r.Log.CurrentTerm(), req.PrevIdx+uint64(len(req.Entries)), true))
	}

	return
}

func (r *replica) processAppendEntriesResponse(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	resp, ok := rpc.(*AppendEntriesResponse)
	if !ok {
		debug.Println("Recieved Invalid AppendRespRPC")
		return
	}

	if resp.Term < r.Log.CurrentTerm() {
		debug.Printf("Rejecting Old appendEntries Response %v < %v\n", resp.Term, r.Log.CurrentTerm())
		return
	}

	// Change to follower and update term if found a higher term and return.
	ok, err = r.stepDown(resp.Term)
	if ok || err != nil {
		return
	}

	if ok := p.processAppendEntriesResponse(resp); ok {
		err = r.leaderCommit()
	}

	return
}

func (r *replica) processSnapRequest(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	req, ok := rpc.(*SnapshotRequest)
	if !ok {
		debug.Println("Recieved Invalid SnapReqRPC")
		return
	}

	if req.Term < r.Log.CurrentTerm() {
		debug.Printf("Old Snapshot Request %v < %v\n", req.Term, r.Log.CurrentTerm())

		// snapPos doesn't matter.
		p.Send(mkSnapshotResponse(r.Log.CurrentTerm(), 0))
		return
	}

	// Change to follower and update term if found a higher term and continue processing.
	ok, err = r.stepDown(req.Term)
	if err != nil {
		return
	}

	// Now the terms match.
	// update leader.
	r.updateLeader(p)

	if r.Snapshotter == nil {
		if r.Snapshotter, err = r.Log.NewSnapshotter(); err != nil {
			return
		}
	}

	wh := r.Snapshotter.GetPosition()
	if req.SnapPos > wh {
		err = fmt.Errorf("snapshot data must be sent ordered")
		return
	} else if req.SnapPos < wh {
		debug.Printf("Old snapshot data (expected %d, received %d); ignoring rpc\n", wh, req.SnapPos)
		p.Send(mkSnapshotResponse(r.Log.CurrentTerm(), wh-1))
		return
	} else {
		if _, err = r.Snapshotter.Write(req.SnapData); err != nil {
			return
		}
	}

	if req.Done {
		if err = r.replaceSnapshot(); err == nil {
			err = r.exec()
		}

		if err != nil {
			return
		}

		r.Snapshotter = nil
	}

	p.Send(mkSnapshotResponse(r.Log.CurrentTerm(), uint64(len(req.SnapData))+req.SnapPos))
	return
}

func (r *replica) readSnapshot() (err error) {
	sr := r.Log.GetSnapshotter()
	if sr == nil {
		return
	}

	s, _, err := r.vfySnapshot(sr)
	if err != nil {
		return
	}

	firstIdx := r.Log.FirstIndex()
	if s.SnapIdx > r.Log.LastIndex() || s.SnapIdx >= firstIdx && r.Log.TermAt(s.SnapIdx) != s.SnapTerm {
		r.Log.TruncateFrom(firstIdx)
		r.respChs.TruncateFrom(firstIdx)
		r.configurator.TruncateFrom(firstIdx)
	}

	if s.SnapIdx >= r.Log.FirstIndex() {
		r.Log.TruncateUpto(s.SnapIdx + 1)
		r.respChs.TruncateUpto(s.SnapIdx + 1)
		r.configurator.TruncateUpto(s.SnapIdx + 1)
	}

	r.configurator.SetSnapshot(s.SnapIdx, s.Configuration)

	r.commitIdx = max(r.commitIdx, s.SnapIdx)

	return
}

func (r *replica) replaceSnapshot() (err error) {
	os, _, err := r.vfySnapshot(r.Log.GetSnapshotter())
	if err != nil {
		return
	}

	ns, _, err := r.vfySnapshot(r.Snapshotter)
	if err != nil || ns == nil {
		return
	}

	if os != nil && ns.SnapIdx <= os.SnapIdx {
		r.discardSnapshotter()
		return
	}

	if err = r.Log.SetSnapshotter(r.Snapshotter); err != nil {
		return
	}

	err = r.readSnapshot()

	return
}

func (r *replica) processSnapResponse(msg rpcMsg) (err error) {
	rpc := msg.RPC
	p := msg.peer

	resp, ok := rpc.(*SnapshotResponse)
	if !ok {
		debug.Println("Recieved Invalid SnapRespRPC")
		return
	}

	// Change to follower and update term if found a higher term and return.
	ok, err = r.stepDown(resp.Term)
	if ok || err != nil {
		return
	}

	p.processSnapshotResponse(resp)

	return
}

// Implements comparable interface.
type indexVal uint64

func (idx1 indexVal) Less(i interface{}) bool {
	idx2, ok := i.(indexVal)
	if !ok {
		debug.Println("BUG!! Less() method on indexVal muste be called with type indexVal")
		return false
	}

	return idx1 < idx2
}

func (r *replica) leaderCommit() (err error) {
	if r.GetState() != Leader {
		debug.Println("BUG!! leaderCommit() Must be called for leaders only")
		return
	}

	val, ok := r.configurator.GetMajority(func(s server) comparable { return indexVal(s.MatchIndex()) }).(indexVal)
	if !ok {
		debug.Println("BUG!! passed type indexVal; must return type indexVal from GetMajority()")
		return
	}
	commitIdx := uint64(val)
	curTerm := r.Log.CurrentTerm()

	// If commitIdx is not advanced ⇒ Return.
	if r.commitIdx >= commitIdx {
		return
	}

	// If we are NOT commiting atleast an entry from current term, We can't safely conclude them to be commited. This might cause waiting for new commands to be issued even when it might be safe to commit them.
	// We resolve this by writing a NOPCommand for current term, which could be commited(along with entries for previous terms), when it is replicated to a majority while this server is still leader.
	if r.Log.TermAt(commitIdx) != curTerm && r.Log.TermAt(r.Log.LastIndex()) != curTerm {
		if _, err = r.writeCommand(MkClientCommand(0, r.self, &NOPCommand{}), nil); err == nil {
			err = r.Log.FlushEntries()
		}
		return
	}

	confIdx := r.configurator.GetIndex(r.Log.LastIndex())

	if commitIdx >= confIdx {
		switch r.configurator.GetState() {
		// Cold,new is commited, create Cnew in log and set Cnew.
		case JOINT:
			if r.cfgMsg.clcmd == nil {
				r.cfgMsg.clcmd = MkClientCommand(0, "", nil)
			}

			conf := &Configuration{
				OldServers: r.configurator.GetCurrentConfig().GetNewServers(),
				NewServers: nil,
			}
			r.cfgMsg.clcmd.cmd = conf

			var cnewIdx uint64
			if cnewIdx, err = r.writeCommand(r.cfgMsg.clcmd, r.cfgMsg.respC); err != nil {
				return
			}

			r.configurator.Set(cnewIdx, conf)

		case STABLE:
			// Cnew commited.
			if r.commitIdx < confIdx {
				r.cfgMsg = clientMsg{}
			}
			// Cnew excludes me.
			if r.ɛCnil() {
				var ok bool
				ok, err = r.stepDown(curTerm + 1)
				if !ok {
					debug.Println("BUG!! Can never happen")
					return
				}
				debug.Println("Safe to kill Server")
			}
		}
	}

	// Update commitIdx.
	r.commitIdx = commitIdx

	// Exec any new commited commands.
	err = r.exec()

	return
}

func (r *replica) exec() (err error) {
	var es []*Entry
	for r.lastApplied < r.commitIdx {
		es, err = r.Log.Entries(r.lastApplied+1, r.commitIdx, uint64(r.maxRequestSize()))
		if err != nil {
			if err == ErrSnapRd {
				var rs *RaftState
				var rd io.Reader
				sr := r.Log.GetSnapshotter()
				rs, rd, err = r.vfySnapshot(sr)
				if err != nil || rs == nil || rd == nil {
					err = fmt.Errorf("bug!! Must have a valid snapshot to restore")
					return
				}

				if err = r.StateMachine.Restore(rd); err != nil {
					return
				}

				// Update lastApplied
				r.lastApplied = rs.SnapIdx
				continue
			} else {
				break
			}
		}

		for _, e := range es {
			var clcmd *ClientCommand
			if clcmd, err = e.clientCommand(); err != nil {
				break
			}

			if !clcmd.isRaftCommand() {
				respC := r.respChs.Remove(r.lastApplied + 1)
				r.wg.Add(1)
				go func() {
					defer r.wg.Done()

					resp := mkClientResponse("", r.StateMachine.Execute(clcmd))
					if respC != nil {
						respC <- resp
					}
				}()
			} else {
				cmd := clcmd.cmd
				switch cmd.(type) {
				case *NOPCommand:
					// Nothing to do.

				case *Configuration:
					if conf := cmd.(*Configuration); conf.GetNewServers() == nil {
						r.writeReply(r.lastApplied+1, mkClientResponse("", errResp{nil}))
						r.configurator.GCServers()
					}

				default:
					debug.Printf("Can't execute Raft Command %+v\n", cmd)
				}
			}

			r.lastApplied++
		}

		if err != nil {
			break
		}
	}

	return
}
