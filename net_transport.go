package raft

import (
	"bufio"
	"encoding/gob"
	"fmt"
	"net"
	"sync"
)

var errRPCType = fmt.Errorf("bug!! RPC kind and type mismatch")

// Unified message to gob encode for all raft rpc.
type message struct {
	MsgType uint8

	*AppendEntriesRequest
	*AppendEntriesResponse

	*RequestVoteRequest
	*RequestVoteResponse

	*SnapshotRequest
	*SnapshotResponse
}

var messagePool = sync.Pool{
	New: func() interface{} { return new(message) },
}

// NetTransporter implements raft.Transporter. Transport using net.Conn.
type NetTransporter struct {
	rid string
	con net.Conn
	wr  *bufio.Writer
	enc *gob.Encoder
	dec *gob.Decoder
}

// Send performs a blocking send of RPC over connection.
func (t *NetTransporter) Send(rpc RPC) (err error) {
	// Allocate/Reuse one.
	m := messagePool.Get().(*message)
	switch rpc.Kind() {
	case AppendReqRPC:
		req, ok := rpc.(*AppendEntriesRequest)
		if !ok {
			return errRPCType
		}
		m.MsgType = AppendReqRPC
		m.AppendEntriesRequest = req

	case AppendRespRPC:
		resp, ok := rpc.(*AppendEntriesResponse)
		if !ok {
			return errRPCType
		}
		m.MsgType = AppendRespRPC
		m.AppendEntriesResponse = resp

	case VoteReqRPC:
		req, ok := rpc.(*RequestVoteRequest)
		if !ok {
			return errRPCType
		}
		m.MsgType = VoteReqRPC
		m.RequestVoteRequest = req

	case VoteRespRPC:
		resp, ok := rpc.(*RequestVoteResponse)
		if !ok {
			return errRPCType
		}
		m.MsgType = VoteRespRPC
		m.RequestVoteResponse = resp

	case SnapReqRPC:
		req, ok := rpc.(*SnapshotRequest)
		if !ok {
			return errRPCType
		}
		m.MsgType = SnapReqRPC
		m.SnapshotRequest = req

	case SnapRespRPC:
		resp, ok := rpc.(*SnapshotResponse)
		if !ok {
			return errRPCType
		}
		m.MsgType = SnapRespRPC
		m.SnapshotResponse = resp

	default:
		err = fmt.Errorf("invalid RPC message Type %d", rpc.Kind())
	}

	if err = t.enc.Encode(m); err != nil {
		return
	}

	err = t.wr.Flush()

	// Free it.
	*m = message{}
	messagePool.Put(m)

	return
}

// Receive receives a new RPC or blocks.
func (t *NetTransporter) Receive() (rpc RPC, err error) {
	// Allocate/Reuse one.
	m := messagePool.Get().(*message)
	if err = t.dec.Decode(m); err != nil {
		debug.Println(err)
		return
	}

	// Ignore Invalid message.
	if m.MsgType >= nRPCs {
		debug.Printf("message from %s(%s) of type %d is invalid\n", t.rid, t.con.RemoteAddr(), m.MsgType)
		return
	}

	switch m.MsgType {
	case AppendReqRPC:
		rpc = m.AppendEntriesRequest
	case AppendRespRPC:
		rpc = m.AppendEntriesResponse
	case VoteReqRPC:
		rpc = m.RequestVoteRequest
	case VoteRespRPC:
		rpc = m.RequestVoteResponse
	case SnapReqRPC:
		rpc = m.SnapshotRequest
	case SnapRespRPC:
		rpc = m.SnapshotResponse

	default:
		debug.Printf("message from %s(%s) of type %d is invalid\n", t.RemoteUID(), t.con.RemoteAddr(), m.MsgType)
	}

	// Free it.
	*m = message{}
	messagePool.Put(m)

	return
}

// RemoteUID returns the unique id of the other end of Transporter.
func (t *NetTransporter) RemoteUID() string {
	return t.rid
}

// Close closes Transporter.
func (t *NetTransporter) Close() (err error) {
	if t.con != nil {
		err = t.con.Close()
	}

	return
}

// NewNetTransporter creates a NetTransporter from a given remote server ID and the connection.
func NewNetTransporter(rid string, c net.Conn) (t Transporter) {
	bo := bufio.NewWriter(c)
	enc := gob.NewEncoder(bo)
	dec := gob.NewDecoder(bufio.NewReader(c))
	t = &NetTransporter{
		rid: rid,
		con: c,
		wr:  bo,
		enc: enc,
		dec: dec,
	}

	return
}
