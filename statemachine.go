package raft

import (
	"io"
)

// Response abstracts the response of state machine after executing ClientCommand.
type Response interface {
	Error() error
}

// StateMachine abstracts a state machine whose state is to be replicated.
type StateMachine interface {
	Execute(*ClientCommand) Response
	Snapshot(io.Writer) error
	Restore(io.Reader) error
}
