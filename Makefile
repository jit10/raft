GOGOPROTOPATH := $(GOPATH)/src/code.google.com/p/gogoprotobuf
PROTOPATH := .
PBFILES := $(wildcard $(PROTOPATH)/*.pb.go)
PROTOFILES := $(wildcard $(PROTOPATH)/*.proto)

.PHONY: proto all

all: proto debug
proto:	protoclean protogen
protoclean:
	rm -f $(PBFILES)
protogen:
	protoc --proto_path=$(GOPATH)/src:$(GOGOPROTOPATH)/protobuf:$(PROTOPATH) --gogo_out=$(PROTOPATH) $(PROTOFILES)
	sed -ri ':a; s%(.*)/\*.*\*/%\1%; ta; /\/\*/ !b; N; ba' $(PBFILES)

release:
	sed 's/debugging[[:space:]]*=[[:space:]]*true/debugging = false/g' debug.gengo > debug.go

debug:
	sed 's/debugging[[:space:]]*=[[:space:]]*false/debugging = true/g' debug.gengo > debug.go
