/*
Package raft is an implementation of raft replication protocol.

Assumptions

It uses encoding/binary package's Varint encoding and protobuf encoding to marshal commands. It is assumed that these encoding scheme are unlikely to change.

Transporter

In our implementation none of the RPCs contain the server ID, as it should be the responsibility of underlying transport layer to convey this information. Transporter interface exposes this via RemoteUID(). The implementation doesn't require Transporter to be an ordered protocol, however we recommend to use an ordered retransmitting protocol for better throughput.

TODO

fslog: should have a cache of most recent commands upto max size.
*/
package raft
