package raft

import (
	"io"
	"sync"
	"time"
)

type peer struct {
	sync.Mutex
	sync.WaitGroup

	voteGranted bool

	nextIdx  uint64
	matchIdx uint64

	nextIdxVirt uint64

	// Membership.
	ɛ

	alive bool
	uid   string
	rpcC  chan RPC

	// Snapshot related book-keeping.
	snapReader io.Reader
	snapPos    uint64
	snapEnd    uint64

	// peer close event.
	quitC chan bool

	// dialer wait channel with buffer size 1. Always initialized to contain value.
	// Also used like a lock to run atmost one dialer goroutine at any time.
	dialC chan bool

	// AppendEntries/Snapshot Request Pipeline.
	æρℓ chan uint64

	// counter of time in Units since last sent AppendEntriesRPC. when HBUnits reaches thresold, we send empty AppendEntriesRPC.
	HBUnits uint32

	// used as timer to read log and send AppendEntriesRPC.
	AEUnits uint32
	AETm    *time.Timer

	// This peer's Transporter.
	Transporter

	// The host replica it's peer of.
	host *replica
}

func (p *peer) Start() {
	// Flush the AE pipeline.
	p.Flushæρℓ()

	// Fork both loops.
	p.Add(2)
	go p.Receiver()
	go p.Sender()
}

func (p *peer) UID() string {
	p.Lock()
	defer p.Unlock()

	return p.uid
}

func (p *peer) isSnapshotting() bool {
	p.Lock()
	defer p.Unlock()

	return p.snapReader != nil
}

func (p *peer) getSnapReader() io.Reader {
	p.Lock()
	defer p.Unlock()

	return p.snapReader
}

func (p *peer) startSnapshotting(rd io.Reader) {
	if rd == nil {
		return
	}
	p.Lock()
	defer p.Unlock()

	p.snapReader = rd
	p.snapPos, p.snapEnd = 0, ^uint64(0)
	p.Flushæρℓ()
}

func (p *peer) stopSnapshotting() {
	p.Lock()
	defer p.Unlock()

	p.snapReader = nil
}

// Status of a peer.
func (p *peer) isDead() bool {
	return !p.alive
}

// Closes existing connection. The only function which marks it dead.
// close() on a dead peer does nothing. This is static function in C lingo.
func (p *peer) close() (err error) {
	// Grab Lock.
	p.Lock()
	defer p.Unlock()

	// If already close() has been called, do nothing.
	if p.isDead() {
		return
	}

	// Close rpcC, quitC and Conn to end the Sender() & Receiver() loop.
	err = p.Transporter.Close()
	close(p.rpcC)
	close(p.quitC)

	p.alive = false

	// stop snapshotting.
	p.snapReader = nil

	return
}

// close() and wait for the goroutines to exit. This  the external interface.
func (p *peer) Close() (err error) {
	err = p.close()
	// Stop timer.
	p.AETm.Stop()

	// Wait for dialer to exit and reset.
	<-p.dialC
	p.dialC <- true

	// Wait for both loops to exit.
	p.Wait()

	return
}

func (p *peer) reset(t Transporter) (err error) {
	// Grab Lock.
	p.Lock()
	defer p.Unlock()

	// Set it alive, set the Conn and Channels.
	if t != nil {
		p.alive = true
	}
	p.Transporter = t
	p.rpcC = make(chan RPC, reqQSize)
	p.quitC = make(chan bool)

	// Reset the timer.
	p.AEUnits = 1
	p.AETm.Reset(p.host.baseTime())
	p.heartBeatReset()

	// Start again.
	p.Start()

	return
}

// Close() the existing and Resets the peer with a new Transporter. This is the external interface.
func (p *peer) Reset(t Transporter) (err error) {
	// First Close() it.
	err = p.Close()
	p.reset(t)
	return
}

// Sender Loop started by NewNetTransport.
func (p *peer) Sender() {
	defer func() {
		p.close()
		p.Done()
	}()

	if p.Transporter == nil {
		return
	}

	var err error
	for {
		var rpc RPC
		var ok bool
		select {
		case rpc, ok = <-p.rpcC:
			if !ok {
				goto senderLoopExit
			}
		}

		// bail out on error.
		if err = p.Transporter.Send(rpc); err != nil {
			debug.Println(err)
			break
		}
	}
senderLoopExit:
}

// Receiver Loop started by Start().
func (p *peer) Receiver() {
	defer func() {
		p.close()
		p.Done()
	}()

	if p.Transporter == nil {
		return
	}

	for {
		rpc, err := p.Transporter.Receive()
		if err != nil {
			debug.Println(err)
			break
		}
		if rpc == nil {
			continue
		}

		select {
		case p.host.msgQ() <- rpcMsg{RPC: rpc, peer: p}:

		case <-p.quitC:
			goto receiverLoopExit
		}
	}
receiverLoopExit:
}

// p.rpcC is Buffered, so requests can be pending, when writing to the Conn takes some time.
func (p *peer) Send(rpc RPC) (ok bool) {
	p.Lock()
	defer p.Unlock()

	if p.isDead() {
		// Try to connect to peer.
		select {
		case <-p.dialC:
			go func() {
				t, err := p.host.Dial(p.uid)
				if err != nil {
					debug.Println(err)
				} else {
					p.close()
					p.reset(t)
				}
				p.dialC <- true
			}()
		default:
		}
		return
	}

	// Non-blocking Send to the channel.
	select {
	case p.rpcC <- rpc:
		ok = true
	default:
	}

	return ok
}

func (p *peer) voteReset() {
	p.Lock()
	defer p.Unlock()

	p.voteGranted = false
}

func (p *peer) ResetState() {
	p.Lock()
	defer p.Unlock()

	p.nextIdx = p.host.LastIndex() + 1
	p.nextIdxVirt = p.nextIdx
	p.matchIdx = 0

	p.voteGranted = false

	p.snapReader = nil

	p.Flushæρℓ()
}

/*
Pipelined AppendEntries:
We can send new appendEntriesRPC without waiting for previous one's response upto pipeline size. Then we block, till we receive response.
This optimistic pipelining should perform better in the common case, where failures are rare.
Our pipeline is just a queue of indices. where enqueued indices grows strictly monotonically increasing. These indices correspond to the index of last Entries of AERequests. Response Index has to match this to note successful Append.
*/

/*
Following Functions are only relevant when p.host is Leader.

Note: There are no locks, since these are called strictly sequentially.
If ever in future the design changes the race detector will show races, which would need fixing. As what we would want is atomicity, not just race-free. (Putting Locks makes the code race-free).
*/

func (p *peer) Flushæρℓ() {
	for {
		select {
		case <-p.æρℓ:
		default:
			return
		}
	}
}

func (p *peer) MatchIndex() uint64 {
	return p.matchIdx
}

func (p *peer) GrantVote() {
	p.voteGranted = true
}

func (p *peer) VoteGranted() bool {
	return p.voteGranted
}

/*
p.AETm timer triggers checking the log for any new log entries to send.
Following AIMD when we can send a Request, the timer decreases multiplicatively by 2 units, else it increases additively by 1. (1 ≤ AEUnits ≤ heartBeatUnits)
*/

func (p *peer) AIMDℴInc() {
	if p.AEUnits >= p.host.heartbeatUnits() {
		return
	}

	p.AEUnits++
}

func (p *peer) ExpℴInc() {
	if p.AEUnits >= p.host.heartbeatUnits() {
		return
	}

	p.AEUnits *= 2
}

func (p *peer) AIMDℴDec() {
	if p.AEUnits == 1 {
		return
	}

	p.AEUnits /= 2
}

func (p *peer) heartBeatIncrease(n uint32) {
	p.HBUnits += n
}

func (p *peer) heartBeatReset() {
	p.HBUnits = 0
}

func (p *peer) heartBeatExpired() bool {
	return p.HBUnits >= p.host.heartbeatUnits()
}

func (p *peer) RequestVote(actual bool) bool {
	p.voteReset()
	h := p.host
	lastIdx := h.LastIndex()
	return p.Send(mkRequestVoteRequest(h.CurrentTerm(), lastIdx, h.TermAt(lastIdx), !actual))
}

func always(b bool, c <-chan time.Time) <-chan time.Time {
	if b {
		c := make(chan time.Time)
		close(c)
		return c
	}

	return c
}

func (p *peer) tryρℓ(rpc RPC, n uint64) bool {
	var ok bool
	select {
	case p.æρℓ <- n:
		if p.Send(rpc) {
			ok = true
		} else {
			<-p.æρℓ
		}

	default:
	}

	return ok
}

func (p *peer) searchρℓ(n uint64) bool {
	var i uint64
	for {
		select {
		case i = <-p.æρℓ:
			if i == n {
				goto searchExit
			}

		default:
			goto searchExit
		}
	}

searchExit:
	return i == n
}

func (p *peer) AppendEntries(force bool) bool {
	h := p.host

	var ok bool
	select {
	case <-always(force, p.AETm.C):
		var es []*Entry
		var err error
		curterm := h.CurrentTerm()

		if !p.isSnapshotting() {
			es, err = h.Entries(p.nextIdxVirt, p.nextIdxVirt+uint64(h.maxEntriesPerRequest()), uint64(h.maxRequestSize()))
			if err != nil {
				if err == ErrSnapRd {
					if sr := h.GetSnapshotter(); sr == nil {
						debug.Println("Bug!! Must have valid snapshot")
						return false
					}

					p.startSnapshotting(h.GetSnapshotter().Reader())
				} else {
					debug.Println(err)
					return false
				}
			} else if len(es) == 0 {
				p.heartBeatIncrease(p.AEUnits)
			}
		}

		if p.isSnapshotting() {
			rd := p.getSnapReader()
			if rd == nil {
				return false
			}

			b := make([]byte, h.maxRequestSize())
			if _, err = rd.Read(b); err != nil && err != io.EOF {
				debug.Println(err)
				p.stopSnapshotting()
			} else {
				var done bool
				if err == io.EOF {
					done = true
				}
				rpc := mkSnapshotRequest(curterm, b, p.snapPos, done)
				if p.tryρℓ(rpc, p.snapPos+uint64(len(b))) {
					// message pipelined to be sent.
					ok = true
					p.snapPos += uint64(len(b))
					if done {
						p.snapEnd = p.snapPos
					}
				}
			}
		} else {
			if len(es) > 0 || len(es) == 0 && (p.heartBeatExpired() || force) {
				rpc := mkAppendEntriesRequest(h.CurrentTerm(), h.TermAt(p.nextIdxVirt-1), p.nextIdxVirt-1, h.commitIndex(), es)

				// No Pipelining for empty messages.
				if len(es) == 0 {
					ok = p.Send(rpc)
				} else {
					if p.tryρℓ(rpc, p.nextIdxVirt+uint64(len(es))-1) {
						// message pipelined to be sent.
						ok = true
						p.nextIdxVirt += uint64(len(es))
					}
				}
			}

			// Queued some message for sending. Reset Heartbeat.
			if ok {
				p.heartBeatReset()
			}
		}

		p.Lock()
		// AIMD adjustment/Exponential backoff timer.
		if ok {
			p.AIMDℴDec()
		} else if p.isDead() {
			p.ExpℴInc()
		} else {
			p.AIMDℴInc()
		}

		// Reset the timer to fire again after p.AEUnits.
		p.AETm.Reset(time.Duration(p.AEUnits) * p.host.baseTime())
		p.Unlock()

	default:
	}

	return ok
}

func (p *peer) processAppendEntriesResponse(resp *AppendEntriesResponse) bool {
	// Failed Response →
	// 1. If resp.Index is smaller than nextIndex then peer is lagging. We start from directly there.
	// 2. Or else there are conflicting entries. We decrement nextIndex by one and try again.
	if !resp.Success {
		if resp.Index < p.nextIdx {
			p.nextIdxVirt, p.nextIdx = resp.Index+1, resp.Index+1
		} else {
			if p.nextIdx > 1 {
				p.nextIdx--
			}
			p.nextIdxVirt = p.nextIdx
		}
		p.Flushæρℓ()

		return false
	}

	// It is success. resp.Index must be within the range.
	if resp.Index < p.nextIdx-1 || resp.Index >= p.nextIdxVirt {
		debug.Printf("Received Response too old/duplicate (got %d, limits(%d, %d))\n", resp.Index, p.nextIdx-1, p.nextIdxVirt)
		return false
	}

	// For occasional response to heartbeat messages.
	if resp.Index == p.nextIdx-1 {
		p.matchIdx = resp.Index
		return true
	}

	// Success → search for the matching sent request in pipeline.
	// If Failed to do this(now pipeline is empty), Bad Response. Reset nextIdxVirt.
	if !p.searchρℓ(resp.Index) {
		debug.Println("Bad response")
		p.nextIdxVirt = p.nextIdx
		return false
	}

	// Found matching request. Update indices.
	p.matchIdx = resp.Index
	p.nextIdx = p.matchIdx + 1

	return true
}

func (p *peer) processSnapshotResponse(resp *SnapshotResponse) bool {
	if !p.isSnapshotting() {
		return false
	}

	ok := p.searchρℓ(resp.SnapPos)

	// bad Response or finished snapshotting ⇒ stop Snapshotting.
	if !ok || resp.SnapPos == p.snapEnd {
		p.stopSnapshotting()
	}

	return ok
}
