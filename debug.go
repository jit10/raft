package raft

import (
	"log"
	"os"
	"runtime"
	"strconv"
)

type debugging bool

var dlog *log.Logger

const debug debugging = true

func init() {
	dlog = log.New(os.Stdout, "", 0)
}

func getCallerPrefix() string {
	var prefix string
	pc, file, line, ok := runtime.Caller(2)
	if ok {
		var i int
		for i = len(file) - 1; i >= 0; i-- {
			if file[i] == '/' {
				break
			}
		}
		if fn := runtime.FuncForPC(pc); fn != nil {
			fname := fn.Name()
			var j int
			for j = len(fname) - 1; j >= 0; j-- {
				if fname[j] == '/' {
					break
				}
			}

			prefix = fname[j+1:] + "@" + file[i+1:] + ":" + strconv.Itoa(line) + ": "
		}
	}

	return prefix
}

func (d debugging) Printf(format string, a ...interface{}) {
	if d {
		dlog.SetPrefix(getCallerPrefix())
		dlog.Printf(format, a...)
	}
}

func (d debugging) Println(a ...interface{}) {
	if d {
		dlog.SetPrefix(getCallerPrefix())
		dlog.Println(a...)
	}
}
