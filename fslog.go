package raft

import (
	"bytes"
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sync/atomic"
)

const (
	maxFilePosition = (1 << 63) - 1

	fileBufferSize  = 64 * 1024
	cacheSize       = 256 * 1024
	maxCacheEntries = 1024

	entriesLog = "entries.log"
	voteLog    = "vote.log"
	termLog    = "term.log"
	snapLog    = "snap.log"
	snapPrefix = "snap."
)

// Each entry costs 16 bytes in memory. (16 * maxEntries) memory is pre-allocated for each replica.
type entry struct {
	term uint64
	epos int64
}

// Implements raft.Log. Used by default.
type fsLog struct {
	path string

	voteID string

	curTerm uint64

	startIdx uint64
	lastIdx  uint64

	entries []entry

	termf *os.File
	votef *os.File

	buf bytes.Buffer
	pos int64

	// The entries log file.
	esf *os.File

	// snapshot
	fsnap Snapshotter
}

func writeByteSlice(b []byte, w io.Writer) (n int, err error) {
	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]

	blen := binary.PutUvarint(bs, uint64(len(b)))

	var k int
	if k, err = w.Write(bs[:blen]); err != nil {
		return
	}
	n += k

	if k, err = w.Write(b); err != nil {
		return
	}
	n += k

	return
}

func readByteSlice(r io.ReaderAt, pos int64) (b []byte, n int, err error) {
	var blen uint64

	/* Read the slice length */
	var ba [binary.MaxVarintLen64]byte
	bs := ba[:]
	k, err := r.ReadAt(bs, pos)

	if k > 0 {
		blen, k = binary.Uvarint(bs[:k])
	} else {
		return
	}

	if k <= 0 {
		err = fmt.Errorf("Failed to read length of entry slice at %d", pos)
		return
	}
	pos += int64(k)

	n = k + int(blen)
	/* Read the entries slice */
	b = make([]byte, blen)
	_, err = r.ReadAt(b, pos)

	return
}

// Entry associated with this index.
func (l *fsLog) entryAt(idx uint64) (e *entry) {
	if idx < l.startIdx || idx > l.lastIdx {
		return nil
	}

	return &l.entries[idx-l.startIdx]
}

/* Implement raft.Log */

// Index of first entry in the log.
func (l *fsLog) FirstIndex() uint64 {
	return atomic.LoadUint64(&l.startIdx)
}

// Last applied log index.
func (l *fsLog) LastIndex() uint64 {
	return atomic.LoadUint64(&l.lastIdx)
}

// Term Associated with this index.
func (l *fsLog) TermAt(index uint64) (term uint64) {
	e := l.entryAt(index)
	if e == nil {
		term = 0
	} else {
		term = e.term
	}

	return
}

func (l *fsLog) CurrentTerm() uint64 {
	return atomic.LoadUint64(&l.curTerm)
}

func (l *fsLog) UpdateCurrentTerm(nterm uint64) error {
	var ba [8]byte
	bs := ba[:]
	binary.LittleEndian.PutUint64(bs, nterm)

	_, err := l.termf.WriteAt(bs, 0)
	if err != nil {
		err = l.termf.Truncate(0)
	} else {
		l.curTerm = nterm
		err = l.termf.Sync()
	}

	return err
}

func (l *fsLog) VotedFor() string {
	return l.voteID
}

func (l *fsLog) UpdateVotedFor(candidateID string) error {
	if l.voteID == candidateID {
		return nil
	}

	_, err := l.votef.WriteAt([]byte(candidateID), 0)
	if err != nil {
		err = l.votef.Truncate(0)
	} else {
		l.voteID = candidateID
		err = l.votef.Sync()
	}

	return err
}

func (l *fsLog) EntryAt(index uint64) (e *Entry, err error) {
	if index < l.startIdx {
		err = ErrSnapRd
		return
	}

	le := l.entryAt(index)
	if le == nil {
		return
	}

	b, _, err := readByteSlice(l.esf, le.epos)
	if err == nil {
		e = new(Entry)
		err = e.Unmarshal(b)
	}

	return
}

func (l *fsLog) Entries(from, to, limit uint64) (entries []*Entry, err error) {
	if from < l.startIdx {
		err = ErrSnapRd
		return
	}

	le := l.entryAt(from)
	if le == nil {
		return
	}

	if to == 0 && limit == 0 {
		err = fmt.Errorf("both (to) index and limit can't be zero")
		return
	}

	idx := from
	var start, end int64
	start = le.epos
	if limit == 0 {
		limit = maxFilePosition - uint64(start)
	}

	for end = start; end < start+int64(limit) && idx <= to && l.entryAt(idx) != nil; idx++ {
		end = l.entryAt(idx).epos
	}
	idx--

	b := make([]byte, end-start)
	if _, err = l.esf.ReadAt(b, start); err != nil {
		return
	}

	lim := uint64(0)
	es := make([]*Entry, 0, idx-from+1)
	bs := b
	for i := from; i < idx; i++ {
		elen, k := binary.Uvarint(bs)
		if k <= 0 {
			err = fmt.Errorf("failed to read length of entry slice for index %d", i)
			break
		}

		e := new(Entry)
		if err = e.Unmarshal(bs[k : uint64(k)+elen]); err != nil {
			break
		}
		es = append(es, e)
		bs = bs[uint64(k)+elen:]
		lim += elen
	}
	if err == nil {
		var lb []byte
		if lb, _, err = readByteSlice(l.esf, end); err == nil && lim+uint64(len(lb)) <= limit {
			e := new(Entry)
			if err = e.Unmarshal(lb); err == nil {
				es = append(es, e)
			}
		}
	}

	entries = es[:len(es)]
	return
}

func (l *fsLog) WriteEntry(e *Entry) (idx uint64, err error) {
	if len(l.entries) == cap(l.entries) {
		err = ErrSnapWr
		return
	}

	eb, err := e.Marshal()
	if err != nil {
		return
	}

	pos := l.pos
	n, _ := writeByteSlice(eb, &l.buf)
	l.pos += int64(n)
	l.entries = append(l.entries, entry{e.Term(), pos})

	if l.buf.Len() >= fileBufferSize {
		err = l.FlushEntries()
	}

	idx = l.startIdx + uint64(len(l.entries)-1)

	return
}

func (l *fsLog) FlushEntries() (err error) {
	n, err := l.buf.WriteTo(l.esf)

	if err != nil {
		off, err := l.esf.Seek(-n, os.SEEK_END)
		if err == nil {
			err = l.esf.Truncate(off)
		}

		// if Seek/Truncate fails panic
		if err != nil {
			panic(err)
		}

		// Reset position and buffer.
		l.pos -= n + int64(l.buf.Len())
		l.buf.Reset()
	} else {
		atomic.StoreUint64(&l.lastIdx, l.startIdx+uint64(len(l.entries)-1))
		err = l.esf.Sync()
	}

	return
}

func (l *fsLog) TruncateFrom(index uint64) (err error) {
	if err = l.FlushEntries(); err != nil {
		return
	}

	e := l.entryAt(index)
	if e == nil {
		return
	}

	// Set write head and truncate.
	off, err := l.esf.Seek(e.epos, os.SEEK_SET)
	if err == nil {
		err = l.esf.Truncate(off)
	}

	if err != nil {
		return
	}

	l.pos = off
	l.entries = l.entries[:index-1]
	atomic.StoreUint64(&l.lastIdx, l.startIdx+uint64(len(l.entries)-1))

	return
}

func (l *fsLog) TruncateUpto(index uint64) (err error) {
	if err = l.FlushEntries(); err != nil {
		return
	}

	e := l.entryAt(index)
	if e == nil {
		return
	}

	// seek to the new position where we want to copy from.
	off, err := l.esf.Seek(e.epos, os.SEEK_SET)
	if err != nil {
		return
	}

	// Create a temp file to copy.
	f, err := ioutil.TempFile(l.path, "temp.")
	if err != nil {
		return
	}

	// copy data to temp file.
	if _, err = io.Copy(f, l.esf); err != nil && err != io.EOF {
		return
	}

	// Sync the temp file to disk.
	if err = f.Sync(); err != nil {
		return
	}

	// close the entries log.
	if err = l.esf.Close(); err != nil {
		return
	}

	// rename the temp file to entries log.
	ofn := f.Name()
	nfn := l.path + string(os.PathSeparator) + entriesLog
	f.Close()
	err = os.Rename(ofn, nfn)

	// Now open the entries log and set.
	if f, err = os.OpenFile(nfn, os.O_RDWR|os.O_CREATE, 0660); err == nil {
		_, err = f.Seek(0, os.SEEK_END)
	}

	// No errors. Now update in-mem slice.
	if err == nil {
		l.esf = f

		nl := uint64(len(l.entries)) - index + 1
		copy(l.entries, l.entries[index-1:])
		l.entries = l.entries[:nl]

		l.startIdx = index
		atomic.StoreUint64(&l.lastIdx, l.startIdx+uint64(len(l.entries)-1))

		// Adjust the file positions.
		for i := range l.entries {
			l.entries[i].epos -= off
		}
	}

	return
}

// Satisfies io.Reader interface.
type fileReader struct {
	*os.File
	pos int64
}

func (rd *fileReader) Read(b []byte) (n int, err error) {
	n, err = rd.File.ReadAt(b, rd.pos)
	rd.pos += int64(n)
	return
}

// Satisfies Snapshotter interface.
type fileSnapshot struct {
	*os.File
}

func (snap fileSnapshot) GetPosition() uint64 {
	pos, err := snap.File.Seek(0, os.SEEK_CUR)
	if err != nil {
		return ^uint64(0)
	}
	return uint64(pos)
}

func (snap fileSnapshot) Write(b []byte) (int, error) {
	return snap.File.Write(b)
}

func (snap fileSnapshot) Close() error {
	return snap.File.Close()
}

func (snap fileSnapshot) Remove() error {
	snap.File.Close()
	return os.Remove(snap.File.Name())
}

func (snap fileSnapshot) Reader() io.Reader {
	return &fileReader{
		File: snap.File,
		pos:  0,
	}
}

func (l *fsLog) NewSnapshotter() (s Snapshotter, err error) {
	f, err := ioutil.TempFile(l.path, "snap.")
	if err != nil {
		return
	}

	s = fileSnapshot{File: f}
	return
}

func (l *fsLog) SetSnapshotter(s Snapshotter) (err error) {
	snap, ok := s.(fileSnapshot)
	if !ok {
		err = fmt.Errorf("BUG!! Snapshotter not recognized")
	}

	ofn := snap.File.Name()
	snap.Close()

	nfn := l.path + string(os.PathSeparator) + snapLog
	if err = os.Rename(ofn, nfn); err != nil {
		return
	}

	f, err := os.OpenFile(nfn, os.O_RDONLY, 0600)
	if err == nil {
		l.fsnap = fileSnapshot{File: f}
	}

	return
}

func (l *fsLog) GetSnapshotter() Snapshotter {
	return l.fsnap
}

func (l *fsLog) Close() (err error) {
	if err = l.votef.Close(); err != nil {
		return
	}

	if err = l.termf.Close(); err != nil {
		return
	}

	if err = l.esf.Close(); err != nil {
		return
	}

	if l.fsnap != nil {
		err = l.fsnap.Close()
	}

	return
}

func (l *fsLog) Checksum() string {
	var b [fileBufferSize]byte
	bs := b[:]

	h := sha1.New()
	pos := int64(0)
	for {
		n, err := l.esf.ReadAt(bs, pos)
		pos += int64(n)
		h.Write(bs[:n])

		if err != nil {
			break
		}
	}
	cksum := h.Sum(nil)

	// Convert to hex string.
	ckstr := make([]byte, hex.EncodedLen(len(cksum)))
	hex.Encode(ckstr, cksum)

	return string(ckstr)
}

/* ... */

func readTerm(f *os.File) (t uint64, err error) {
	var b [8]byte
	bs := b[:]
	n, err := f.ReadAt(bs, 0)
	if err == nil {
		t = binary.LittleEndian.Uint64(bs)
	} else if n == 0 && err == io.EOF {
		err = nil
	}

	return
}

func readVotedFor(f *os.File) (candidateID string, err error) {
	fi, err := f.Stat()
	if err == nil {
		b := make([]byte, fi.Size())
		if _, err = f.ReadAt(b, 0); err == nil {
			candidateID = string(b)
		}
	}
	return
}

// Open a log on file system with maxEntries to snapshot.
func openDefaultLog(path string, maxEntries uint32) (lg Log, err error) {
	_, err = os.Stat(path)
	if err != nil && !os.IsNotExist(err) {
		return
	}

	if err = os.MkdirAll(path, 0755); err != nil {
		return
	}

	// open snapshot if there; startIndex
	startIdx := uint64(1)

	path += string(os.PathSeparator)
	var vl, tl, el, sl *os.File
	var fsnap Snapshotter

	// open fsnap in RDONLY.
	if sl, err = os.OpenFile(path+snapLog, os.O_RDONLY, 0660); err == nil {
		fsnap = fileSnapshot{File: sl}
	} else if err != nil && !os.IsNotExist(err) {
		return
	}

	// open vote, term log with O_SYNC.
	if vl, err = os.OpenFile(path+voteLog, os.O_RDWR|os.O_SYNC|os.O_CREATE, 0660); err != nil {
		return
	}
	if tl, err = os.OpenFile(path+termLog, os.O_RDWR|os.O_SYNC|os.O_CREATE, 0660); err != nil {
		return
	}

	// open entries log.
	var pos int64
	if el, err = os.OpenFile(path+entriesLog, os.O_RDWR|os.O_CREATE, 0660); err == nil {
		pos, err = el.Seek(0, os.SEEK_END)
	}
	if err != nil {
		return
	}

	var cterm uint64
	if cterm, err = readTerm(tl); err != nil {
		return
	}

	var voteID string
	if voteID, err = readVotedFor(vl); err != nil {
		return
	}

	l := &fsLog{
		path:     path,
		voteID:   voteID,
		curTerm:  cterm,
		startIdx: startIdx,
		entries:  make([]entry, 0, maxEntries),
		votef:    vl,
		termf:    tl,
		pos:      pos,
		esf:      el,
		fsnap:    fsnap,
	}

	var rpos int64
	for {
		var e Entry
		b, n, err := readByteSlice(l.esf, rpos)
		if err == nil {
			if err = e.Unmarshal(b); err == nil {
				l.entries = append(l.entries, entry{term: e.Term(), epos: rpos})
				rpos += int64(n)
			}
		}

		if err != nil {
			if err == io.EOF {
				err = nil
			} else {
				debug.Printf("Error: %v; truncating to previous good entry position %d", err, pos)
				if err = l.esf.Truncate(pos); err != nil {
					panic(err)
				}
			}
			break
		}
	}
	l.lastIdx = l.startIdx + uint64(len(l.entries)-1)

	lg = l
	return
}
