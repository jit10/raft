package raft

// Communication is a interface to provide communication service to raft server.
type Communication interface {
	String() string
	// Dial a remote server.
	Dial(string) (Transporter, error)
	// Accept waits for and returns an incoming Transporter.
	// Calling Accept on closed communication MUST return error.
	Accept() (Transporter, error)
	// Multiple calls on Close() MUST be well-behaved.
	Close() error
}

// Transporter is a link between two raft servers.
// Send() / Receive() is called sequentially.
// Send() / Receive() MUST return error when called on closed transporter.
type Transporter interface {
	// Get the unique ID of the remote end.
	RemoteUID() string
	// Send a rpc to the other end.
	Send(RPC) error
	// Receive a rpc from other end.
	// MUST block when no RPC is received.
	Receive() (RPC, error)
	// Multiple calls on Close() MUST be well-behaved.
	Close() error
}
