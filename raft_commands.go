package raft

// Command Ids Reserved by Raft.
const (
	NopCMD = iota
	AddCMD
	DelCMD
	ReconfCMD
	ConfigCMD

	ReservedIDs
)

// ID implements Command.
func (c *NOPCommand) ID() uint8 {
	return NopCMD
}

// ID implements Command.
func (c *AddServerCommand) ID() uint8 {
	return AddCMD
}

// ID implements Command.
func (c *DelServerCommand) ID() uint8 {
	return DelCMD
}

// ID implements Command.
func (c *ReconfigureCommand) ID() uint8 {
	return ReconfCMD
}

// ID implements Command.
func (c *Configuration) ID() uint8 {
	return ConfigCMD
}
