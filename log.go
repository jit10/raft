package raft

import (
	"fmt"
	"io"
)

var (
	// ErrSnapWr represents the error condition to trigger raft log compaction.
	ErrSnapWr = fmt.Errorf("failure to write entry; Max log entries reached")
	// ErrSnapRd represents the error condition to trigger transfer of snapshots to followers.
	ErrSnapRd = fmt.Errorf("failed to read entry; Index is already snapshotted")
)

// Snapshotter reresents a log snapshot.
type Snapshotter interface {
	// Write position.
	GetPosition() uint64
	io.WriteCloser
	Reader() io.Reader
	Remove() error
}

// Log abstracts the basic functionality of raft log used by the protocol.
type Log interface {
	// LastIndex returns the index of last entry in the log.
	LastIndex() uint64
	// FirstIndex returns the index of first entry in the log.
	FirstIndex() uint64

	// TermAt returns the term of the log entry at index. Must return 0 for failure. since terms count from 1.
	TermAt(index uint64) (term uint64)
	// CurrentTerm returns the current term used by the replica.
	CurrentTerm() uint64
	// UpdateCurrentTerm updates the current term to nterm.
	UpdateCurrentTerm(nterm uint64) error

	// VotedFor returns CandidateID voted for the current term.
	VotedFor() string
	// UpdateVotedFor updates the candidateID voted for current term.
	UpdateVotedFor(candidateID string) error

	// EntryAt returns the log entry at index. MUST return ErrSnapRd if (index) is snapshotted.
	EntryAt(index uint64) (e *Entry, err error)
	// Entries returns log entries starting from (from) index with a max size of (limit), if possible and upto (to) index.
	// Either of (to) Or (limit) could be zero to indicate optional. Both being zero returns error. MUST return ErrSnapRd if (from) is snapshotted.
	Entries(from, to, limit uint64) (entries []*Entry, err error)

	// WriteEntry writes the ENtry to log and returns the corresponding index and error, if any.
	// MUST return ErrSnapWr to enforce snapshotting. This is the only way to initiate snapshotting.
	WriteEntry(e *Entry) (idx uint64, err error)
	// FlushEntries flushes any buffered entries to persistent log. Optional.
	FlushEntries() (err error)

	// TruncateFrom truncates the log from index(including) to the end of log.
	TruncateFrom(index uint64) (err error)
	// TruncateUpto truncates all entries from the beginning upto index(excluding).
	TruncateUpto(index uint64) (err error)

	// NewSnapshotter creates a new snapshot.
	NewSnapshotter() (Snapshotter, error)
	// GetSnapshotter returns the current snpashot.
	GetSnapshotter() Snapshotter
	// SetSnapshotter sets the current snapshot.
	SetSnapshotter(Snapshotter) error

	// Checksum returns the checksum of the entries log. MUST be collision resistant.
	Checksum() string

	// Close closes the Log.
	Close() (err error)
}
