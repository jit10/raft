package raftTest

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"bitbucket.org/jpathy/raft"
)

const (
	maxDialTries = 100
	maxClient    = 10
)

func verboseln(args ...interface{}) {
	if testing.Verbose() {
		fmt.Println(args)
	}
}

type pingRPC string

func (rpc pingRPC) Kind() uint8 {
	return raft.ReservedIDs + 1
}

type pongRPC string

func (rpc pongRPC) Kind() uint8 {
	return raft.ReservedIDs + 2
}

func serve(uid string, t raft.Transporter) {
	defer t.Close()

	for {
		if rpc, err := t.Receive(); err != nil {
			verboseln(err)
			break
		} else {
			ping, ok := rpc.(pingRPC)
			if ok {
				verboseln(uid, "Received ping,", ping)

				pong := pongRPC(uid)
				if err := t.Send(pong); err != nil {
					verboseln(err)
					break
				}
				verboseln(uid, "Sent Pong", t.RemoteUID())
			} else {
				break
			}
		}
	}
}

func dialAndSend(comm raft.Communication, lid string, rid string) {
	var err error
	var t raft.Transporter

	for i := 0; i < maxDialTries; i++ {
		t, err = comm.Dial(rid)
		if err == nil {
			break
		}
		time.Sleep(time.Millisecond)
	}

	if err != nil || t == nil {
		return
	}

	for {
		ping := pingRPC(lid)
		if err := t.Send(ping); err != nil {
			verboseln(err)
			break
		}
		verboseln(lid, "Sent Ping", t.RemoteUID())

		var rpc raft.RPC
		if rpc, err = t.Receive(); err != nil {
			verboseln(err)
			break
		}
		if _, ok := rpc.(pongRPC); ok {
			verboseln(lid, "Received pong", t.RemoteUID())
		} else {
			break
		}
	}

	t.Close()
}

func client(uid string, comm raft.Communication, srvs []string) {
	var wg sync.WaitGroup

	wg.Add(len(srvs))
	for _, s := range srvs {
		s := s
		go func() {
			dialAndSend(comm, uid, s)
			wg.Done()
		}()
	}

	wg.Wait()
}

func server(uid string, comm raft.Communication) {
	var wg sync.WaitGroup

	ts := make([]raft.Transporter, 0, maxClient)
	for {
		if len(ts) >= cap(ts) {
			break
		}

		t, err := comm.Accept()
		if err == nil {
			ts = append(ts, t)
			wg.Add(1)
			go func() {
				defer wg.Done()
				serve(uid, t)
			}()
		} else {
			verboseln(err)
			break
		}
	}

	for i := range ts {
		ts[i].Close()
	}

	wg.Wait()
}

func TestChanCommunication(t *testing.T) {
	srvs := []string{"s1", "s2", "s3"}
	cls := []string{"s4", "s5"}
	comms := make([]raft.Communication, 0, len(srvs)+len(cls))

	var wg sync.WaitGroup

	wg.Add(len(srvs) + len(cls))

	for _, s := range srvs {
		s := s
		go func() {
			defer wg.Done()

			if c, err := newChanComm(s); err == nil {
				comms = append(comms, c)
				server(s, c)
			}
		}()
	}

	for _, s := range cls {
		s := s
		go func() {
			defer wg.Done()

			if c, err := newChanComm(s); err == nil {
				comms = append(comms, c)
				client(s, c, srvs)
			}
		}()
	}

	time.Sleep(15 * time.Second)
	for i := range comms {
		comms[i].Close()
	}

	wg.Wait()
}
