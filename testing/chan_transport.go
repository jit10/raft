package raftTest

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"bitbucket.org/jpathy/raft"
)

const (
	chanTmOut  = time.Duration(30 * time.Second)
	dropChance = 1.0 / 0.00000002
)

var (
	errClosed = fmt.Errorf("Closed transporter")
	errTmout  = fmt.Errorf("Timed out; Closing")
)

type chanTransporter struct {
	sync.Mutex
	closed bool
	rid    string
	sendC  chan raft.RPC
	recvC  chan raft.RPC
	closeC chan bool
}

func (t *chanTransporter) isClosed() bool {
	t.Lock()
	defer t.Unlock()

	return t.closed
}

func (t *chanTransporter) setClosed() {
	t.Lock()
	defer t.Unlock()

	t.closed = true
}

func (t *chanTransporter) RemoteUID() string {
	return t.rid
}

func (t *chanTransporter) Send(rpc raft.RPC) (err error) {
	if t.isClosed() {
		err = errClosed
		return
	}

	if rand.Intn(dropChance) == 0 {
		//		return
	}

	select {
	case t.sendC <- rpc:

	case <-t.closeC:
		t.setClosed()
		err = errClosed

	case <-time.After(chanTmOut):
		t.setClosed()
		err = errTmout
	}

	return
}

func (t *chanTransporter) Receive() (rpc raft.RPC, err error) {
	if t.isClosed() {
		err = errClosed
		return
	}

receiveLoop:
	select {
	case rpc = <-t.recvC:

	case <-t.closeC:
		t.setClosed()
		err = errClosed

	case <-time.After(chanTmOut):
		goto receiveLoop
	}

	return
}

func (t *chanTransporter) Close() error {
	t.Lock()
	defer t.Unlock()

	t.closed = true
	select {
	// Already closed.
	case <-t.closeC:

	default:
		close(t.closeC)
	}

	return nil
}
