package raftTest

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"sync"
	"testing"
	"time"

	"bitbucket.org/jpathy/raft"
)

var (
	errBogCmd = fmt.Errorf("Bogus Command")
	errEmpty  = fmt.Errorf("Empty Response")
)

// For our testing purpose the clients can interact with the database replicas directly from the following map.
// In practice the exposed interface could be over network and clients would need a way to find identity atleast one of the db replica.(could be done by name service).

var dblck sync.Mutex
var dbs map[string]*DB

func init() {
	dbs = make(map[string]*DB)
}

func addDB(uid string, db *DB) {
	dblck.Lock()
	defer dblck.Unlock()

	dbs[uid] = db
}

func getDB(uid string) *DB {
	dblck.Lock()
	defer dblck.Unlock()

	return dbs[uid]
}

func (cmd *PutCommand) ID() uint8 {
	return raft.ReservedIDs
}

type PutResponse struct {
	error
}

func (resp PutResponse) Error() error {
	return resp.error
}

type DB struct {
	sync.Mutex
	kv map[string]string
	raft.Server
}

func NewDB() *DB {
	return &DB{
		kv: make(map[string]string),
	}
}

func (db *DB) Join(uid string) error {
	resp := db.Server.Add(uid, time.Second)
	if resp != nil {
		return resp.Response.Error()
	}

	return errEmpty
}

func (db *DB) Reconf(srvs []string) (*DB, error) {
	resp := db.Server.Reconfigure(srvs, time.Second)
	if resp != nil {
		return getDB(resp.LeaderID()), resp.Response.Error()
	}

	return nil, errEmpty
}

func (db *DB) Stop() error {
	return db.Server.Stop()
}

func (db *DB) Execute(clcmd *raft.ClientCommand) (resp raft.Response) {
	put, ok := clcmd.Command().(*PutCommand)
	if !ok {
		return PutResponse{errBogCmd}
	}

	db.Lock()
	defer db.Unlock()

	db.kv[put.K] = put.V
	return PutResponse{nil}
}

func (db *DB) Snapshot(w io.Writer) (err error) {
	return json.NewEncoder(w).Encode(db.kv)
}

func (db *DB) Restore(r io.Reader) (err error) {
	return json.NewDecoder(r).Decode(db.kv)
}

// This is the client interface for cluster of DBs.
// Since our db.Execute() is idempotent, we don't track clientIds and their sequence numbers in Execute().
// For non-idempotent commands it would be necessary for both client and statemachine to track sequence numbers.
func Put(leaderID, k string, v string) {
	var ldrdb *DB
	var respC <-chan *raft.ClientResponse
	clcmd := raft.MkClientCommand(0, "", &PutCommand{K: k, V: v})
	err := fmt.Errorf("")
	for err != nil {
		if ldrdb == nil {
			ldrdb = getDB(leaderID)
		}
		respC, err = ldrdb.Server.Issue(clcmd)
		if err == nil {
			select {
			case resp := <-respC:
				err = resp.Response.Error()
				if err == raft.ErrNotLeader {
					ldrdb = getDB(resp.LeaderID())
				}
			case <-time.After(500 * time.Millisecond):
				err = fmt.Errorf("Time out")
			}
		} else {
			time.Sleep(10 * time.Millisecond)
		}
	}
}

var defaultParams = raft.ConfigParams{
	ServerID:     "",
	BatchLimit:   100,
	MaxReqSize:   64 * 1024,
	MaxEntries:   1024,
	BaseTimeUnit: 1000, // 1ms
	ElectTmUnits: 200,
	LogPath:      "/tmp/_raft",
}

func dbInstance(uid string) (db *DB, err error) {
	params := defaultParams
	params.ServerID = uid
	params.LogPath += params.ServerID
	db = NewDB()
	s, err := raft.NewServer(&params, db, newChanComm, nil)
	if err != nil {
		return
	}

	db.Server = s

	return
}

func TestRaft(t *testing.T) {
	if err := raft.RegisterCommands(&PutCommand{}); err != nil {
		t.Error(err)
	}

	srvs := []string{"s1", "s2", "s3", "s4", "s5"}

	nsrvs := []string{"s6", "s7", "s8"}

	var wg sync.WaitGroup
	wg.Add(len(srvs))

	bsC := make(chan bool, len(srvs))
	for _, s := range srvs {
		s := s
		go func() {
			defer wg.Done()
			db, err := dbInstance(s)
			if err != nil {
				t.Error(err)
				return
			}
			addDB(s, db)

			if db.Server.CanBootstrap() {
				if s == "s1" {
					if err = db.Bootstrap(); err != nil {
						return
					}
					db.Server.Start()
				} else {
					db.Server.Start()
					var ldrdb *DB
					for ldrdb == nil {
						ldrdb = getDB("s1")
						time.Sleep(10 * time.Microsecond)
					}

					err = ldrdb.Join(s)
					for err != nil {
						err = ldrdb.Join(s)
						fmt.Println(err)
						time.Sleep(2 * time.Second)
					}
				}
			}
			bsC <- true
		}()
	}

	for i := 0; i < len(srvs); i++ {
		<-bsC
	}
	wg.Add(len(nsrvs))
	for _, s := range nsrvs {
		s := s
		go func() {
			defer wg.Done()
			db, err := dbInstance(s)
			if err != nil {
				t.Error(err)
				return
			}
			addDB(s, db)

			if db.Server.CanBootstrap() {
				db.Server.Start()
			}
		}()
	}

	wg.Wait()

	// Reconfigure to contain only the new 3 servers.
	var ldrdb *DB
	ldrdb = getDB("s1")

	ldrdb, err := ldrdb.Reconf(nsrvs)
	for err != nil {
		if ldrdb == nil {
			ldrdb = getDB("s1")
		}
		ldrdb, err = ldrdb.Reconf(nsrvs)
		time.Sleep(2 * time.Second)
	}

	// After reconfiguration try issuing put requests.
	for i := 1; i < 100; i++ {
		Put("s6", strconv.Itoa(i), strconv.Itoa(i*2))
	}

	// Give some time to replicate all.
	time.Sleep(time.Second)

	// make sure all three servers log matches.
	cks := make([]string, len(nsrvs))
	for i, s := range nsrvs {
		cks[i] = getDB(s).Server.LogChecksum()
		if i > 0 && cks[i] != cks[i-1] {
			t.Error("Log checksum mismatch between %s %s", nsrvs[i], nsrvs[i-1])
			break
		}
	}

	// Stop all of them.
	dblck.Lock()
	for _, db := range dbs {
		db.Server.Stop()
	}
	dblck.Unlock()

	// Now start all three in configuration.
	for _, s := range nsrvs {
		getDB(s).Server.Start()
	}

	// Give some time to settle down.
	time.Sleep(time.Second)

	// Finally shutdown.
	dblck.Lock()
	for _, db := range dbs {
		db.Server.Stop()
	}
	dblck.Unlock()
}
