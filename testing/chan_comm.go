package raftTest

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/jpathy/raft"
)

const (
	bufSize   = 1 << 5
	dialTmOut = time.Duration(30 * time.Millisecond)
)

var (
	errExists = fmt.Errorf("Failed to reach server")
	errQuit   = fmt.Errorf("closed communication")
)

var commLck sync.Mutex
var commMap map[string]*chanComm

func init() {
	commMap = make(map[string]*chanComm)
}

type handshake struct {
	uid    string
	sendC  chan raft.RPC
	recvC  chan raft.RPC
	closeC chan bool
}

type chanComm struct {
	uid string
	lC  chan handshake
	qC  chan bool
}

func (comm *chanComm) String() string {
	return "chan"
}

func (comm *chanComm) Dial(raddr string) (t raft.Transporter, err error) {
	commLck.Lock()
	rcomm, ok := commMap[raddr]
	commLck.Unlock()

	if !ok {
		err = errExists
		return
	}

	hs := handshake{
		uid:    comm.uid,
		sendC:  make(chan raft.RPC, bufSize),
		recvC:  make(chan raft.RPC, bufSize),
		closeC: make(chan bool),
	}

	select {
	case rcomm.lC <- hs:
	case <-time.After(dialTmOut):
		err = errExists
		return
	case <-comm.qC:
		err = errQuit
		return
	}

	// Now switch sendC and recvC for me.
	hs.sendC, hs.recvC = hs.recvC, hs.sendC
	t = newChanTransporter(rcomm.uid, hs)
	return
}

func (comm *chanComm) Accept() (t raft.Transporter, err error) {
	var hs handshake

acceptLoop:
	select {
	case hs = <-comm.lC:

	case <-comm.qC:
		err = errQuit
		return

	case <-time.After(10 * time.Millisecond):
		goto acceptLoop
	}

	t = newChanTransporter(hs.uid, hs)
	return
}

func (comm *chanComm) Close() error {
	select {
	// Already closed.
	case <-comm.qC:
		return nil

	default:
		close(comm.qC)

		commLck.Lock()
		delete(commMap, comm.uid)
		commLck.Unlock()
	}

	return nil
}

func newChanTransporter(rid string, hs handshake) raft.Transporter {
	return &chanTransporter{
		rid:    rid,
		sendC:  hs.sendC,
		recvC:  hs.recvC,
		closeC: hs.closeC,
	}
}

func newChanComm(uid string) (raft.Communication, error) {
	comm := &chanComm{
		uid: uid,
		lC:  make(chan handshake),
		qC:  make(chan bool),
	}

	commLck.Lock()
	commMap[uid] = comm
	commLck.Unlock()

	return comm, nil
}
