# Instruction #

go test runs both TestChanCommunication and TestRaft.

To Simulate only a cluster of raft servers.

	go test -run TestRaft

Delete all the logs(/tmp/\_raft\*) before running go test again. Logs are not cleaned to be examined by the user.
